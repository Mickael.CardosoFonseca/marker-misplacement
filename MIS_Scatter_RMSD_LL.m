function S = MIS_Scatter_RMSD_LL(R, Er, Leg_length)

Angle_name = {'LHip_flex', 'LHip_add', 'LHip_rot', 'LKnee_flex', 'LKnee_add', 'LKnee_rot', 'LAnkle_flex', 'LAnkle_add', 'LAnkle_rot'};
top = {'Leg_length','RMSD_Error_5mm', 'RMSD_Error_10mm', 'RMSD_Error_15mm', 'RMSD_Error_20mm','RMSD_Error_30mm'};
Magnitude = {'Misp_5', 'Misp_10', 'Misp_15', 'Misp_20', 'Misp_30'};

% Normalize leg length
for i = 1:length(Leg_length)
    for ii = 1:length(Er)
       Leg_norm(i,ii) = (Er(ii)*10)/Leg_length(i); % 10*5 mat
    end
end
figure (12)
hold on
% RMSD values for ANT * Er * patients
for j = 1:length(Angle_name)
    for ii = 1:length(Leg_length)
        for i = 1:length(Magnitude)
            RMSD(i,:) = R.(Angle_name{j}).Ant.(Magnitude{i}).RMSD(ii);
        end
        RMSD = RMSD';
        % Plot scatter
        s = subplot(3,3,j);
        title(char(Angle_name{j}));
        ylabel('RMSD (�)');
        xlabel(['% of Leg Length' ]);
        s = scatter(Leg_norm(j,:), RMSD);
        hold on
        clear RMSD
    end
end
end