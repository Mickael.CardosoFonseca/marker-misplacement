function mis_bplot(Angles, Error, Er, MM, counter, neg_dir, pos_dir)
mat_2dir = [];
Angles_AA = {'180_30_AP_ML', '180_20_AP_ML', '180_15_AP_ML', '180_10_AP_ML', '180_5_AP_ML', '0_5_AP_ML', '0_10_AP_ML','0_15_AP_ML','0_20_AP_ML', '0_30_AP_ML'};
RMSD_hipflex   = [];
for j = 1:length(Angles_AA)
    Angle_AA = strcat('LHipAngles_LKNE_', char(Angles_AA(j)));
    for i = 1:counter
        L_Hip = Angles(i).original.LHipAngles_PyCGM1;
        L_Hip_err = Angles(i).error.(char(Angle_AA));
        
        % Calculate RMSD for each angle and each error
        RMSD_hipflex(:,i)   =  sqrt((L_Hip_err(:,1) - L_Hip(:,1)).^2);
%         RMSD_hipadd(:,j)    =  sqrt((L_Hip_err(:,2) - L_Hip(:,2)).^2);
%         RMSD_hiprot(:,j)    =  sqrt((L_Hip_err(:,3) - L_Hip(:,3)).^2);
    end
    mat_2dir_flex = mean()
end
end
