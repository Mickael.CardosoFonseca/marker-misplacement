function [Correl, C, Table2] = MIS_Correlation_all_pat(Leg_length, R, Er, x_lim, y_lim, Anthro_param)
    
    Angle_name = {'LHip_flex', 'LHip_add', 'LHip_rot', 'LKnee_flex', 'LKnee_add', 'LKnee_rot', 'LAnkle_flex', 'LAnkle_add', 'LAnkle_rot'};
    top = {'Leg_length','RMSD_Error_5mm', 'RMSD_Error_10mm', 'RMSD_Error_15mm', 'RMSD_Error_20mm','RMSD_Error_30mm'};
    direction = {'Ant', 'Prox','Post', 'Dist'};
    count = 0;
    c = 1;
    for i = 1:length(Leg_length)
        % Vector with % Leg length for all patients
        for j = 1:length(Er)
            norm_err(j+count) = (Er(j)*10)/Leg_length(i);
        end
        count = count +5;
        % Vectors with RMSD for all patients and for each angle and
        % direction
        for ii = 1:length(direction)
            for iii = 1:length(Angle_name)
                RMSD.(direction{ii}).(Angle_name{iii})(c)= R.(Angle_name{iii}).(direction{ii}).Misp_5.RMSD(i);
                RMSD.(direction{ii}).(Angle_name{iii})(c+1)= R.(Angle_name{iii}).(direction{ii}).Misp_10.RMSD(i);
                RMSD.(direction{ii}).(Angle_name{iii})(c+2)= R.(Angle_name{iii}).(direction{ii}).Misp_15.RMSD(i);
                RMSD.(direction{ii}).(Angle_name{iii})(c+3)= R.(Angle_name{iii}).(direction{ii}).Misp_20.RMSD(i);
                RMSD.(direction{ii}).(Angle_name{iii})(c+4)= R.(Angle_name{iii}).(direction{ii}).Misp_30.RMSD(i);
            end
        end
        c = c +5;
    end  
    x = norm_err;
    % Calculate correlation (R, p-valuem, interception and slope)
    for ii = 1:length(Angle_name)
        col = 1;
        for iii = 1:4
            figure(iii)
            s = subplot(3,3,ii)
            s = scatter(norm_err,RMSD.(direction{iii}).(Angle_name{ii}))
            xlim([0 6])
            ylim([0 30])
            
            [Corr.(Angle_name{ii}).(direction{iii}).R, Corr.(Angle_name{ii}).(direction{iii}).p_value] = corrcoef(norm_err,RMSD.(direction{iii}).(Angle_name{ii}));
            y = RMSD.(direction{iii}).(Angle_name{ii});
            b1=x/y;
            P = polyfit(x,y,1);
            Corr.(Angle_name{ii}).(direction{iii}).slope = P(1);
            Corr.(Angle_name{ii}).(direction{iii}).intercept = P(2);
            
            Tab_Corr.(Angle_name{ii})(col) = Corr.(Angle_name{ii}).(direction{iii}).R(1,2);
            Tab_Corr.(Angle_name{ii})(col+1) = Corr.(Angle_name{ii}).(direction{iii}).p_value(1,2);
            Tab_Corr.(Angle_name{ii})(col+2) = Corr.(Angle_name{ii}).(direction{iii}).slope;
            Tab_Corr.(Angle_name{ii})(col+4) = Corr.(Angle_name{ii}).(direction{iii}).intercept;
            
            P1  = polyfit(x(1:5),y(1:5),1);
            P2  = polyfit(x(6:10),y(6:10),1);
            P3  = polyfit(x(11:15),y(11:15),1);
            P4  = polyfit(x(16:20),y(16:20),1);
            P5  = polyfit(x(21:25),y(21:25),1);
            P6  = polyfit(x(26:30),y(26:30),1);
            P7  = polyfit(x(31:35),y(31:35),1);
            P8  = polyfit(x(36:40),y(36:40),1);
            P9  = polyfit(x(41:45),y(41:45),1);
            P10 = polyfit(x(46:50),y(46:50),1);
            
            m_SD = std([P1(1),P2(1),P3(1),P4(1),P5(1),P6(1),P7(1),P8(1),P9(1),P10(1)])
            Tab_Corr.(Angle_name{ii})(col+3) = m_SD;
            col = col +5;
        end
    end
    
    
Var_Names = {string('R'), string('P_value'), string('Slope'), string('Intercept')}; Var_Names = repmat(Var_Names,1,4);
Table_Corr = table(Tab_Corr.LHip_flex', Tab_Corr.LHip_add', Tab_Corr.LHip_rot', Tab_Corr.LKnee_flex', Tab_Corr.LKnee_add', Tab_Corr.LKnee_rot', Tab_Corr.LAnkle_flex', Tab_Corr.LAnkle_add', Tab_Corr.LAnkle_rot');
Table_Corr.Properties.VariableNames = {'HipFlexExt','HipAddAbd', 'HipRot', 'KneeFlexExt', 'KneeAddAbd', 'KneeRot', 'AnkleFlexExt', 'AnkleAddAbd', 'AnkleRot'};
Table_Corr.Properties.RowNames = {'RANT','P_valueANT','SlopeANT','InterceptANT','RPROX','P_valuePROX','SlopePROX','InterceptPROX','RPOST','P_valuePOST','SlopePOST','InterceptPOST','RDIST','P_valueDIST','SlopeDIST','InterceptDIsT'};
writetable(Table_Corr, 'Data_Correlation_SD.xlsx')
end