function Update_UserSettings(MARKER_MIS)
% UPDATE SUFFIX 
fid = fopen('CGM1_1.userSettings','r');
bb = 1;
tline = fgetl(fid);
AA{bb} = tline;
while ischar(tline)
    bb = bb+1;
    tline = fgetl(fid);
    AA{bb} = tline;
end
fclose(fid);
change = (['    Point suffix: ',strcat(MARKER_MIS)]); % Change cell AA
AA{36} = strjoin(cellstr(change));

% Write cell AA into txt
fid = fopen('CGM1_1.userSettings', 'w');
for t = 1:numel(AA)
    if AA{t+1} == -1
        fprintf(fid,'%s', AA{t});
        break
    else
        fprintf(fid,'%s\n', AA{t});
    end
end
fclose(fid);
end