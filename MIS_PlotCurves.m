function Curves = MIS_PlotCurves(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle)
   
%% 1. Normalize gait cycle
    acq = btkReadAcquisition(strcat(data_path, C3D_filename));
    ff = btkGetFirstFrame(acq);
    events = btkGetEvents(acq);
    event_ff = (round(events.Left_Foot_Strike(1)*100))-ff+1;
    event_lf = round(events.Left_Foot_Strike(2)*100)-ff+1;
    
    time = 0:100;

    %% 2. Original angles
    L_Angles = Angles(subject).original.(strcat('L', Angle, 'Angles_PyCGM1'));
    
    for j = 1:3
        subplot(3,1,j)
        L_Angle_x = interp1 (L_Angles(event_ff:event_lf,j), 1:101);
        plot(time,L_Angle_x, 'b')
        hold on
    end
    for m = 1: length(Er)
        for i = 1:length(MM)
            L_er = Angles(subject).error.(strcat('L',Angle,'Angles_', MM{m,i}));
            L_Angle(:,1) = interp1 (L_er(event_ff:event_lf,1), 1:101);
            subplot(3,1,1)
            plot(time,L_Angle(:,1), 'Color', LineColor{m})
            title(strcat(Angle, ' Flex/Extension'))
            ylim([-35 70])
            hold on
            L_Angle(:,2) = interp1 (L_er(event_ff:event_lf,2), 1:101);
            subplot(3,1,2)
            ylim([-35 70])
            plot(time,L_Angle(:,2), 'Color', LineColor{m})
            title(strcat(Angle, ' Add/Abduction'))
            L_Angle(:,3) = interp1 (L_er(event_ff:event_lf,3), 1:101);
            subplot(3,1,3)
            ylim([-35 70])
            plot(time, L_Angle(:,3), 'Color', LineColor{m})
            title(strcat(Angle, ' Int/External Rotation'))
        end
    end
    legend('Original','Misp 5mm', 'Misp 10mm', 'Misp 15mm', 'Misp 20mm', 'Misp 30mm');

end

