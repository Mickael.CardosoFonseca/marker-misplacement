% Create a misplacement of a specific marker and recompute kinematic data
% with PyCGM2
% Author: M.Fonseca # April 2019
close all
clear all
clc


%% 1. Create virtual marke
% rs with Defined error - Define before computation
% MAR1: marker to misplace.
% SEGMENT: segment where the marker is used to define the LCS.
% Er: error magnitudes in mm.
% Error_dir: 'AP_ML' antero-posteior + medial lateral (e.g. LKNE, LANK);'AP_DP' antero-posteior + proximal distal; 'ML_PD' medial lateral + proximal distal (e.g Lasi);
% seg_origin: segment origin
tic
MARK1 = ('LKNE');
Er = [0, 5, 10, 15, 20, 30];
Error_dir = 'AP_ML';

SEGMENT.name = ('LFEMUR');
SEGMENT.origin = ('LHJC');
SEGMENT.proximal = [SEGMENT.name, '_Z'];
SEGMENT.lateral  = [SEGMENT.name, '_Y'];
SEGMENT.anterior = [SEGMENT.name, '_X'];
b=1;
angle = [0; 45; 90; 135; 180; 225; 270; 315];

%% 2. Open .c3d files
% for each patient, open  1 static and 1 gait trial

original_datapath = 'D:\Marker Misplacement Simulation\Test\Original_Data\';
data_path  = 'D:\Marker Misplacement Simulation\Test\Test_2\';
patients   = dir([data_path,'*.c3d']);
[C3D_filenames, C3D_path, FilterIndex]=uigetfile({'*.C3D'},'Sélectionner les ficihers C3D ou GCD',['D:\Marker Misplacement Simulation\Test\Original_Data\' '/'],'MultiSelect','on');
cd 'D:\Marker Misplacement Simulation\Test\Original_Data\'

% Move and rename files
for i=1:length(C3D_filenames)
    copyfile(char(C3D_filenames{i}),data_path);
    C3D_filenames{i} = char(C3D_filenames{i});
end

%% 3. Compute Marker Misplacement + Kinematics
[Angles, MM, counter, Error] = MIS_Computation(C3D_filenames, MARK1, SEGMENT, Er, Error_dir, angle, b, data_path);
save Angles
cd 'D:\Marker Misplacement Simulation\Test\Test_2\'
%% 4. Calculate RMSD 
% compute the results and export RMSD, std and max and mean(over patients)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_RMSD(Angles, Error, Er, MM, counter); 

[RR, Table1] = MIS_table_RMSD(Angles, Error, Er, MM, counter, data_path, C3D_filenames); 
writetable(Table1, 'Table Results RMSD.xls');

%% 5. Polar Plot
Lable = {'     Ant', 'Ant + Dist', 'Dist', 'Post + Dist', 'Post', 'Post + Prox', 'Prox', 'Ant + Prox'};
LineColor = {'b', 'c', 'g', 'm', 'r'};
LineStyle = {'no', ':'};
LevelNum = 5;
maximo = 17;
figure(1)
MIS_PlotPolar(m_RMSD_pMagn, Error)

%% 6. Plot Curves of one subject 
% Containing only one direction 
C3D_filename = '01_02941_04528_20171211-GBNNN-VDEF-06.C3D';
Angle1 = 0;
Angle2 = 180;
subject = 1;

figure(10)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Hip', 'Ant Post')

figure(11)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Knee', 'Ant Post')

figure(12)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Ankle', 'Ant Post')

% or all combined
figure (4)
MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Hip', 'Ant Post', 1)
MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Knee', 'Ant Post', 2)
MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Ankle', 'Ant Post', 3)
% MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'FootProgress', 'Ant Post', 3)
%% 7.  Scatter Plot

anthro = MIS_anthropometric_data(C3D_filenames, data_path);                 % get anthropometric data
 Leg_length = zeros(1, length(anthro.subject));
 Knee_Width = zeros(1, length(anthro.subject));
 for i = 1:length(anthro.subject)
     Leg_length(i) = str2double(anthro.subject(i).Left_LegLength_mm);
     Knee_Width(i) = str2double(anthro.subject(i).Left_KneeWidth_mm);
 end
 
 % Case 1: Regression for each patient. Plot all regressions. Table with
 % mean values from the overral regressions
 figure(2)
 hold on
 for i = 1:10
    [correl_LL, C, Table2] = MIS_Correlation(Leg_length(i), R, Er, 6, 30, 'Leg length', i);   % correl = correlation between  error and leg_length normalized; norm_errors:
 end
 
 figure (3)
 % Case 2: Combine all values and perform a general regression. 
direction = 'Ant'; 
x_lim = 6;
y_lim = 30;
[Correl_all, C, Table_Correl] = MIS_Correlation_all(Leg_length, R, Er, x_lim, y_lim, counter, direction)

 
%% 7. Plot original angles (mean +- SD) of one patient, expected angle (mean + expected angle for % leg length), angle error reported

% patient 1
acq = btkReadAcquisition(strcat('D:\Marker Misplacement Simulation\Test\Test_2\', '01_03000_04617_20180423-GBNNN-VDEF-05.C3D'));
[events, eventsInfo] = btkGetEvents(acq);
Ang = btkGetAngles(acq);

% n_cycles = length(events.Left_Foot_Strike)-1;
% c= 1;
ff= btkGetFirstFrame(acq);
% for i=1:n_cycles
%    angles_ref(i).cycle(:,:) = Ang.LHipAngles_PyCGM1(c:(events.Left_Foot_Strike(i+1)*100)-ff,:);  
%    angles_ref(i).cycle(:,1) = interpol(angles_ref(i).cycle(:,1),100);
%    c = (events.Left_Foot_Strike(i+1)*100)-ff+1;
% end

% angles_hip(:,:) = Ang.LHipAngles_PyCGM1((events.Left_Foot_Strike(1)*100-ff):(events.Left_Foot_Strike(2)*100-ff+1),:)
% Hip Ant
figure(13)
slope_Ant = [1.9, 0.68, 3.91, 2.79, 1.89, 0.65, 0.99, 0.36, 2.6];
intercept_Ant = [0.09, -0.33, 0.57, 0.47, 0.07, -0.16, 0.11,0.35, 1.13];
MIS_PlotCurvePrediction(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, 'MARK1', 'Hip', slope_Ant, intercept_Ant)


% Compute mean and std of all gait cycles relative to one patient
[C3D_filenames, C3D_path]=uigetfile({'*.C3D'},'Sélectionner les ficihers C3D ou GCD',['D:\Marker Misplacement Simulation\Test\Patient_1\' '/'],'MultiSelect','on');
[static_file]=uigetfile({'*.C3D'},'Sélectionner les ficihers C3D ou GCD',['D:\Marker Misplacement Simulation\Test\Patient_1\' '/'],'MultiSelect','on');
Mean_Gait_Cycles2(C3D_filenames, C3D_path, static_file)

% C3D_filenames = {'03000_04617_20180423-GBNNN-VDEF-01.c3d', '03000_04617_20180423-GBNNN-VDEF-03', '03000_04617_20180423-GBNNN-VDEF-04', '03000_04617_20180423-GBNNN-VDEF-05', '03000_04617_20180423-GBNNN-VDEF-06'};
% C3D_path = {'D:\Marker Misplacement Simulation\Test\Patient_1\'};

[C3D_filenames, C3D_path]=uigetfile({'*.C3D'},'Select the dynamic files',['D:\Marker Misplacement Simulation\Test\Patient_1\' '/'],'MultiSelect','on');
[static_file]=uigetfile({'*.C3D'},'Select the static file',['D:\Marker Misplacement Simulation\Test\Patient_1\' '/'],'MultiSelect','on');
[mean, SD] = Mean_Gait_Cycles3(C3D_filenames, C3D_path, static_file)   