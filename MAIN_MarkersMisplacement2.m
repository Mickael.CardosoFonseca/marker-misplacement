% Create a misplacement of a specific set of markers and recompute kinematic data
% with PyCGM2
% Author: M.Fonseca # October 2019
close all
clear all
clc

%% 1. Create virtual marke
% rs with Defined error - Define before computation
% MAR1: marker to misplace.
% SEGMENT: segment where the marker is used to define the LCS.
% Er: error magnitudes in mm.
% Error_dir: 'AP_ML' antero-posteior + medial lateral (e.g. LKNE, LANK);'AP_DP' antero-posteior + proximal distal; 'ML_PD' medial lateral + proximal distal (e.g Lasi);
% seg_origin: segment origin
tic
MARK_to_MIS = {'LASI'};
Er = [5, 10, 15, 20, 30];
Error_dir = 'ML_DP';

SEGMENT.name = ('PELVIS');
SEGMENT.origin = ('midASIS');
SEGMENT.proximal = [SEGMENT.name, '_Z'];
SEGMENT.lateral  = [SEGMENT.name, '_Y'];
SEGMENT.anterior = [SEGMENT.name, '_X'];
b=1;
angle = [0; 90; 180; 270];

%% 2. Open .c3d files
% for each patient, open  1 static and 1 gait trial

original_datapath = 'D:\Marker Misplacement Simulation\Test\Original_Data\';
data_path  = 'D:\Marker Misplacement Simulation\Test\Pelvic Misplacement\';
patients   = dir([data_path,'*.c3d']);
[C3D_filenames, C3D_path, FilterIndex]=uigetfile({'*.C3D'},'Sélectionner les ficihers C3D ou GCD',['D:\Marker Misplacement Simulation\Test\Original_Data\' '/'],'MultiSelect','on');
cd 'D:\Marker Misplacement Simulation\Test\Original_Data\'

% Move and rename files
for i=1:length(C3D_filenames)
    copyfile(char(C3D_filenames{i}),data_path);
    C3D_filenames{i} = char(C3D_filenames{i});
end


%% 3. Compute Marker Misplacement + Kinematics
[Angles, MM, counter, Error] = MIS_Computation3(C3D_filenames, MARK_to_MIS, SEGMENT, Er, Error_dir, angle, b, data_path);
save Angles
cd 'D:\Marker Misplacement Simulation\Test\Pelvic Misplacement\'

%% 4. Calculate RMSD 
% compute the results and export RMSD, std and max and mean(over patients)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_RMSD(Angles, Error, Er, MM, counter); 

[RR, Table1] = MIS_table_RMSD(Angles, Error, Er, MM, counter, data_path, C3D_filenames); 
writetable(Table1, 'Table Results RMSD.xls');

%% 5. Polar Plot
Lable = {'     Ant', 'Ant + Dist', 'Dist', 'Post + Dist', 'Post', 'Post + Prox', 'Prox', 'Ant + Prox'};
LineColor = {'b', 'c', 'g', 'm', 'r'};
LineStyle = {'no', ':'};
LevelNum = 5;
maximo = 17;
figure(1)
MIS_PlotPolar(m_RMSD_pMagn, Error)