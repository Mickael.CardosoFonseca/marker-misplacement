function M = MIS_PlotCurvePrediction(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, marker, joint, slope, intercept)
% Plot kinematic data for each direction for one angle joint   

% Input:
% Angles: Kinematic data
% MM: names misplacements
% subject : number of the subject
% Angle1 and Angle2: values of the angles (�) to plot
% joint: name of the joint to plot kinematics
% direct: direction (for the plot title)

%% 1. Normalize gait cycle
    acq = btkReadAcquisition(strcat(data_path, C3D_filename));
    ff = btkGetFirstFrame(acq);
    events = btkGetEvents(acq);
    event_ff = (round(events.Left_Foot_Strike(1)*100))-ff+1;
    event_lf = round(events.Left_Foot_Strike(2)*100)-ff+1;
    time = 0:100;
%% 2. Original angles
    L_Angles_Hip = Angles(subject).original.LHipAngles_PyCGM1;
    L_Angles_Knee = Angles(subject).original.LKneeAngles_PyCGM1;
    L_Angles_Ankle = Angles(subject).original.LAnkleAngles_PyCGM1;
    for j = 1:3
        % Hip
        subplot(3,3,j)
        L_Angle_x = interp1 (L_Angles_Hip(event_ff:event_lf,j), 1:101);
        plot(time,  L_Angle_x, 'k')
        xlim([0 100])
        ylim([-20 65])
        if j == 1
           title('Hip Flexion-Extension')
           ylabel('Angle (�)')
        elseif j == 2
            title('Hip Adduction-Abduction')
        elseif j == 3
            title('Hip Internal-External Rotation')
        end
        hold on
        y = slope(j)*1.79 + intercept(j);
        if Angles(subject).error.LHipAngles_LKNE_0_10_AP_ML(event_ff,j)>L_Angles_Hip(event_ff,j)
            plot(time,  L_Angle_x + y, 'r')
        else
            plot(time,  L_Angle_x - y, 'r')
        end
        plot(time, interp1(Angles(subject).error.LHipAngles_LKNE_0_10_AP_ML(event_ff:event_lf,j), 1:101), 'LineStyle','--', 'Color', 'b')
        if j == 3
            legend('Original', 'Predicted', 'Simulated')
        end
        
        % Knee
        subplot(3,3,j+3)
        L_Angle_x = interp1 (L_Angles_Knee(event_ff:event_lf,j), 1:101);
        plot(time,  L_Angle_x, 'k')
        xlim([0 100])
        ylim([-20 65])
        if j == 1
            title('Knee Flexion-Extension')
            ylabel('Angle (�)')
        elseif j == 2
            title('Knee Adduction-Abduction')
        elseif j == 3
            title('Knee Internal-External Rotation')
        end
        hold on
        y = slope(j+3)*1.79 + intercept(j+3);
        if Angles(subject).error.LKneeAngles_LKNE_0_10_AP_ML(event_ff,j)>L_Angles_Knee(event_ff,j)
            plot(time,  L_Angle_x + y, 'r')
        else
            plot(time,  L_Angle_x - y, 'r')
        end
        plot(time, interp1(Angles(subject).error.LKneeAngles_LKNE_0_10_AP_ML(event_ff:event_lf,j), 1:101), 'LineStyle','--', 'Color', 'b')
        
        % Ankle
        subplot(3,3,j+6)
        L_Angle_x = interp1 (L_Angles_Ankle(event_ff:event_lf,j), 1:101);
        plot(time,  L_Angle_x, 'k')
        xlim([0 100])
        ylim([-20 65])
        if j == 1
           title('Ankle Flexion-Extension')
           ylabel('Angle (�)')
           xlabel('Gait cycle (%)')
        elseif j == 2
            title('Ankle Adduction-Abduction')
            xlabel('Gait cycle (%)')
        elseif j == 3
            title('Ankle Internal-External Rotation')
            xlabel('Gait cycle (%)')
        end
        hold on
        y = slope(j+6)*1.79 + intercept(j+6);
        if Angles(subject).error.LAnkleAngles_LKNE_0_10_AP_ML(event_ff,j)>L_Angles_Ankle(event_ff,j)
            plot(time,  L_Angle_x + y, 'r')
        else
            plot(time,  L_Angle_x - y, 'r')
        end
        plot(time, interp1(Angles(subject).error.LAnkleAngles_LKNE_0_10_AP_ML(event_ff:event_lf,j), 1:101), 'LineStyle','--', 'Color', 'b')
    end

end
