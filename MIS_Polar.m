function MIS_Polar(m_RMSD_pMagn, Error)

% Plot Polar Plot

HipFlex_1 = vec2mat(m_RMSD_pMagn(:,1),length(Error));
HipFlex_1 = [HipFlex_1(:,1) fliplr(HipFlex_1(:,2:8))];
HipAdd_1  = vec2mat(m_RMSD_pMagn(:,2),length(Error));
HipAdd_1 = [HipAdd_1(:,1) fliplr(HipAdd_1(:,2:8))];
HipRot_1  = vec2mat(m_RMSD_pMagn(:,3),length(Error));
HipRot_1 = [HipRot_1(:,1) fliplr(HipRot_1(:,2:8))];

KneeFlex_1 = vec2mat(m_RMSD_pMagn(:,4),length(Error));
KneeFlex_1 = [KneeFlex_1(:,1) fliplr(KneeFlex_1(:,2:8))];
KneeAdd_1  = vec2mat(m_RMSD_pMagn(:,5),length(Error));
KneeAdd_1 = [KneeAdd_1(:,1) fliplr(KneeAdd_1(:,2:8))];
KneeRot_1  = vec2mat(m_RMSD_pMagn(:,6),length(Error));
KneeRot_1 = [KneeRot_1(:,1) fliplr(KneeRot_1(:,2:8))];

AnkleFlex_1 = vec2mat(m_RMSD_pMagn(:,7),length(Error));
AnkleFlex_1 = [AnkleFlex_1(:,1) fliplr(AnkleFlex_1(:,2:8))];
AnkleAdd_1  = vec2mat(m_RMSD_pMagn(:,8),length(Error));
AnkleAdd_1 = [AnkleAdd_1(:,1) fliplr(AnkleAdd_1(:,2:8))];
AnkleRot_1  = vec2mat(m_RMSD_pMagn(:,9),length(Error));
AnkleRot_1 = [AnkleRot_1(:,1) fliplr(AnkleRot_1(:,2:8))];

MIS_PlotPolar(m_RMSD_pMagn, Error)



end