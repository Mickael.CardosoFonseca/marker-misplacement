function PlotError_for_one_patient(mean_original, sd_original, kin_ant, kin_post, ymin, ymax, title_plot, MAG, n_subplot)
figure(10)
subplot(3,3,n_subplot)
color = ['b', 'c', 'g', 'm', 'r'];
for mag = 1:5
   if mag == 1
       corridor(mean_original, sd_original, 'k', [0:100])
       hold on
       plot(mean_original, 'k')
   end
    values_ant = kin_ant.(MAG{mag});
    mean_res_ant = mean(values_ant,2);
    plot(mean_res_ant, 'Color', color(mag))
    values_post = kin_post.(MAG{mag});
    mean_res_post = mean(values_post,2);
    plot(mean_res_post, 'LineStyle', '--', 'Color', color(mag))
end
    xlim([0 100])
    ylim([ymin ymax])
    ylabel('Angle (�)')
    title(string(title_plot))
end