% Export table containing age and height

data_path  = 'D:\Marker Misplacement Simulation\Test\Static_data';
patients   = dir([data_path,'*.c3d']);
[C3D_filenames,C3D_path, FilterIndex]=uigetfile({'*.C3D'},'Sélectionner les ficihers C3D ou GCD',[data_path '/'],'MultiSelect','on');

for i = 1:length(C3D_filenames)
    file = [C3D_path C3D_filenames{i}];
    C3D.filename = C3D_filenames;
    C3D.pathname = C3D_path;
    acq=btkReadAcquisition(strcat(C3D_path,C3D_filenames{i}));
    temp=btkGetMetaData(acq);
    C3D.MetaData=temp;
    a=struct();
    if isfield(temp.children,'SUBJECTS')==1
        a=fieldnames(temp.children.SUBJECTS.children);
        C3D.SubjectParam(i).Filenames    = C3D_filenames{i};
        C3D.SubjectParam(i).Age          = temp.children.SUBJECTS.children.AGE.info.values;
        C3D.SubjectParam(i).A_Height_mm  = temp.children.SUBJECTS.children.A_Height_mm.info.values;
    end
end

TT = struct2table(C3D.SubjectParam)
writetable(TT, 'Subject_Parameters.xls')
