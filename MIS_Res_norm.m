function [T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_norm(Angles, Error, Er, MM, dynamic_file, data_path, ss)
    

    time = 0:100;
    
    ii = 10;
    direction   = {'Ant', 'Ant_Prox', 'Prox', 'Post_Prox', 'Post', 'Post_Dist', 'Dist', 'Ant_Dist'};
% for ii = 1:length(dynamic_list)
    
    %% 1. Normalize gait cycle
    acq = btkReadAcquisition(strcat(data_path, dynamic_file));
    ff = btkGetFirstFrame(acq);
    events = btkGetEvents(acq);

    if string(ss) == 'cycle'
        event_ff = round(events.Left_Foot_Strike(1)*100)-ff+1;
        event_lf = round(events.Left_Foot_Strike(2)*100)-ff+1;
    elseif string(ss) == 'stance'
        event_ff = round(events.Left_Foot_Strike(1)*100)-ff+1;
        if events.Left_Foot_Strike(1) > events.Left_Foot_Off(1) 
            event_lf = round(events.Left_Foot_Off(2)*100)-ff+1;
        else 
            event_lf = round(events.Left_Foot_Off(1)*100)-ff+1;
        end
    elseif string(ss) == 'swing'
        if events.Left_Foot_Strike(1) > events.Left_Foot_Off(1)
            event_ff = round(events.Left_Foot_Off(2)*100)-ff+1;
            event_lf = round(events.Left_Foot_Strike(2)*100)-ff+1;
        else
            event_ff = round(events.Left_Foot_Off(1)*100)-ff+1;
            event_lf = round(events.Left_Foot_Strike(2)*100)-ff+1;
        end
    end
    
    
    RMSD_hipflex   = [];
    RMSD_hipadd    = [];
    RMSD_hiprot    = [];
    RMSD_kneeflex  = [];
    RMSD_kneeadd   = [];
    RMSD_kneerot   = [];
    RMSD_ankleflex = [];
    RMSD_ankleadd  = [];
    RMSD_anklerot  = [];
    %% 2. Original angles
    L_Hip = Angles(ii).original.LHipAngles_PyCGM1;
    L_Knee = Angles(ii).original.LKneeAngles_PyCGM1;
    L_Ankle = Angles(ii).original.LAnkleAngles_PyCGM1;
    %% 3. Calculate RMSD
    for j = 1:length(Error)
        for k = 1:length(Er)
            EE = ['Misp_',num2str(Er(k))];
            Hip   = strcat('LHipAngles_', string(MM(k,j)));
            Knee  = strcat('LKneeAngles_', string(MM(k,j)));
            Ankle = strcat('LAnkleAngles_', string(MM(k,j)));
            
            L_Hip_err = Angles(ii).error.(char(Hip));
            L_Knee_err = Angles(ii).error.(char(Knee));
            L_Ankle_err = Angles(ii).error.(char(Ankle));
            
            % Calculate RMSD for each angle and each error
            RMSD_hipflex(:,j)   =  sqrt((L_Hip_err(:,1) - L_Hip(:,1)).^2);
            RMSD_hipadd(:,j)    =  sqrt((L_Hip_err(:,2) - L_Hip(:,2)).^2);
            RMSD_hiprot(:,j)    =  sqrt((L_Hip_err(:,3) - L_Hip(:,3)).^2);
            RMSD_kneeflex(:,j)  =  sqrt((L_Knee_err(:,1) - L_Knee(:,1)).^2);
            RMSD_kneeadd(:,j)   =  sqrt((L_Knee_err(:,2) - L_Knee(:,2)).^2);
            RMSD_kneerot(:,j)   =  sqrt((L_Knee_err(:,3) - L_Knee(:,3)).^2);
            RMSD_ankleflex(:,j) =  sqrt((L_Ankle_err(:,1) - L_Ankle(:,1)).^2);
            RMSD_ankleadd(:,j)  =  sqrt((L_Ankle_err(:,2) - L_Ankle(:,2)).^2);
            RMSD_anklerot(:,j)  =  sqrt((L_Ankle_err(:,3) - L_Ankle(:,3)).^2);         
            
            % RMSD
            R.LHip_flex.(direction{j}).(EE).RMSD(ii)   =  mean(RMSD_hipflex(:,j));
            R.LHip_add.(direction{j}).(EE).RMSD(ii)    =  mean(RMSD_hipadd(:,j));
            R.LHip_rot.(direction{j}).(EE).RMSD(ii)    =  mean(RMSD_hiprot(:,j));
            R.LKnee_flex.(direction{j}).(EE).RMSD(ii)  =  mean(RMSD_kneeflex(:,j));
            R.LKnee_add.(direction{j}).(EE).RMSD(ii)   =  mean(RMSD_kneeadd(:,j));
            R.LKnee_rot.(direction{j}).(EE).RMSD(ii)   =  mean(RMSD_kneerot(:,j));
            R.LAnkle_flex.(direction{j}).(EE).RMSD(ii) =  mean(RMSD_ankleflex(:,j));
            R.LAnkle_add.(direction{j}).(EE).RMSD(ii)  =  mean(RMSD_ankleadd(:,j));
            R.LAnkle_rot.(direction{j}).(EE).RMSD(ii)  =  mean(RMSD_anklerot(:,j));
            
            % max RMSD
            R.LHip_flex.(direction{j}).(EE).max_RMSD(ii)   =  max(RMSD_hipflex(:,j));
            R.LHip_add.(direction{j}).(EE).max_RMSD(ii)    =  max(RMSD_hipadd(:,j));
            R.LHip_rot.(direction{j}).(EE).max_RMSD(ii)    =  max(RMSD_hiprot(:,j));
            R.LKnee_flex.(direction{j}).(EE).max_RMSD(ii)  =  max(RMSD_kneeflex(:,j));
            R.LKnee_add.(direction{j}).(EE).max_RMSD(ii)   =  max(RMSD_kneeadd(:,j));
            R.LKnee_rot.(direction{j}).(EE).max_RMSD(ii)   =  max(RMSD_kneerot(:,j));
            R.LAnkle_flex.(direction{j}).(EE).max_RMSD(ii) =  max(RMSD_ankleflex(:,j));
            R.LAnkle_add.(direction{j}).(EE).max_RMSD(ii)  =  max(RMSD_ankleadd(:,j));
            R.LAnkle_rot.(direction{j}).(EE).max_RMSD(ii)  =  max(RMSD_anklerot(:,j));
            
            % standard deviation RMSD
            R.LHip_flex.(direction{j}).(EE).SD(ii)   =  std(RMSD_hipflex(:,j));
            R.LHip_add.(direction{j}).(EE).SD(ii)    =  std(RMSD_hipadd(:,j));
            R.LHip_rot.(direction{j}).(EE).SD(ii)    =  std(RMSD_hiprot(:,j));
            R.LKnee_flex.(direction{j}).(EE).SD(ii)  =  std(RMSD_kneeflex(:,j));
            R.LKnee_add.(direction{j}).(EE).SD(ii)   =  std(RMSD_kneeadd(:,j));
            R.LKnee_rot.(direction{j}).(EE).SD(ii)   =  std(RMSD_kneerot(:,j));
            R.LAnkle_flex.(direction{j}).(EE).SD(ii) =  std(RMSD_ankleflex(:,j));
            R.LAnkle_add.(direction{j}).(EE).SD(ii)  =  std(RMSD_ankleadd(:,j));
            R.LAnkle_rot.(direction{j}).(EE).SD(ii)  =  std(RMSD_anklerot(:,j));
            
            
        end
    end
% end
%% 3. Create and export table .xls
Err_cmp = [];
Magn = [];

Hip_flex_RMSD = [];
Hip_add_RMSD = [];
Hip_rot_RMSD = [];
Knee_flex_RMSD = [];
Knee_add_RMSD = [];
Knee_rot_RMSD = [];
Ankle_flex_RMSD = [];
Ankle_add_RMSD = [];
Ankle_rot_RMSD = [];

Hip_flex_max  = [];
Hip_add_max  = [];
Hip_rot_max  = [];
Knee_flex_max  = [];
Knee_add_max  = [];
Knee_rot_max  = [];
Ankle_flex_max  = [];
Ankle_add_max  = [];
Ankle_rot_max  = [];

Hip_flex_SD = [];
Hip_add_SD = [];
Hip_rot_SD = [];
Knee_flex_SD = [];
Knee_add_SD = [];
Knee_rot_SD = [];
Ankle_flex_SD = [];
Ankle_add_SD = [];
Ankle_rot_SD = [];

Mag   = fieldnames(R.LHip_flex.Ant);
Error = fieldnames(R.LHip_flex);

for j = 1:length(Error)
    A = [repmat(Error(j),1,length(Mag))];
    Err_cmp = [Err_cmp A];
    
        for k = 1:length(Er)
        Hip_flex_RMSD   = [Hip_flex_RMSD; R.LHip_flex.(Error{j}).(Mag{k}).RMSD];
        Hip_add_RMSD    = [Hip_add_RMSD; R.LHip_add.(Error{j}).(Mag{k}).RMSD]; 
        Hip_rot_RMSD    = [Hip_rot_RMSD; R.LHip_rot.(Error{j}).(Mag{k}).RMSD];  
        Knee_flex_RMSD  = [Knee_flex_RMSD; R.LKnee_flex.(Error{j}).(Mag{k}).RMSD];
        Knee_add_RMSD   = [Knee_add_RMSD; R.LKnee_add.(Error{j}).(Mag{k}).RMSD];
        Knee_rot_RMSD   = [Knee_rot_RMSD; R.LKnee_rot.(Error{j}).(Mag{k}).RMSD];  
        Ankle_flex_RMSD = [Ankle_flex_RMSD; R.LAnkle_flex.(Error{j}).(Mag{k}).RMSD];
        Ankle_add_RMSD  = [Ankle_add_RMSD; R.LAnkle_add.(Error{j}).(Mag{k}).RMSD];
        Ankle_rot_RMSD  = [Ankle_rot_RMSD; R.LAnkle_rot.(Error{j}).(Mag{k}).RMSD];
        
        Hip_flex_max    = [Hip_flex_max; R.LHip_flex.(Error{j}).(Mag{k}).max_RMSD];
        Hip_add_max     = [Hip_add_max; R.LHip_add.(Error{j}).(Mag{k}).max_RMSD];
        Hip_rot_max     = [Hip_rot_max; R.LHip_rot.(Error{j}).(Mag{k}).max_RMSD];
        Knee_flex_max   = [Knee_flex_max; R.LKnee_flex.(Error{j}).(Mag{k}).max_RMSD];
        Knee_add_max    = [Knee_add_max; R.LKnee_add.(Error{j}).(Mag{k}).max_RMSD];
        Knee_rot_max    = [Knee_rot_max; R.LKnee_rot.(Error{j}).(Mag{k}).max_RMSD];
        Ankle_flex_max  = [Ankle_flex_max; R.LAnkle_flex.(Error{j}).(Mag{k}).max_RMSD];
        Ankle_add_max   = [Ankle_add_max; R.LAnkle_add.(Error{j}).(Mag{k}).max_RMSD];
        Ankle_rot_max   = [Ankle_rot_max; R.LAnkle_rot.(Error{j}).(Mag{k}).max_RMSD];

        Hip_flex_SD   = [Hip_flex_SD; R.LHip_flex.(Error{j}).(Mag{k}).SD];
        Hip_add_SD    = [Hip_add_SD; R.LHip_add.(Error{j}).(Mag{k}).SD]; 
        Hip_rot_SD    = [Hip_rot_SD; R.LHip_rot.(Error{j}).(Mag{k}).SD];  
        Knee_flex_SD  = [Knee_flex_SD; R.LKnee_flex.(Error{j}).(Mag{k}).SD];
        Knee_add_SD   = [Knee_add_SD; R.LKnee_add.(Error{j}).(Mag{k}).SD];
        Knee_rot_SD   = [Knee_rot_SD; R.LKnee_rot.(Error{j}).(Mag{k}).SD];  
        Ankle_flex_SD = [Ankle_flex_SD; R.LAnkle_flex.(Error{j}).(Mag{k}).SD];
        Ankle_add_SD  = [Ankle_add_SD; R.LAnkle_add.(Error{j}).(Mag{k}).SD];
        Ankle_rot_SD  = [Ankle_rot_SD; R.LAnkle_rot.(Error{j}).(Mag{k}).SD];
        
        Magn = [Magn Mag(k)];
    end
end
An = fieldnames(R);
for i = 1:length(An)
    c=0;
    for j = 1:length(Error)
        for k = 1:length(Mag)
           c = c+1; 
           m_RMSD(c,i) =  mean(R.(An{i}).(Error{j}).(Mag{k}).RMSD);
           m_std(c,i)  =  mean(R.(An{i}).(Error{j}).(Mag{k}).SD);
           m_max_RMSD(c,i)  =  mean(R.(An{i}).(Error{j}).(Mag{k}).max_RMSD);
        end
    end
end
T_RMSD = table(Err_cmp',Magn',Hip_flex_RMSD, Hip_add_RMSD, Hip_rot_RMSD, Knee_flex_RMSD, Knee_add_RMSD, Knee_rot_RMSD, Ankle_flex_RMSD, Ankle_add_RMSD, Ankle_rot_RMSD);
T_std = table(Err_cmp',Magn',Hip_flex_SD, Hip_add_SD, Hip_rot_SD, Knee_flex_SD, Knee_add_SD, Knee_rot_SD, Ankle_flex_SD, Ankle_add_SD, Ankle_rot_SD);
T_max  = table(Err_cmp',Magn',Hip_flex_max, Hip_add_max, Hip_rot_max, Knee_flex_max, Knee_add_max, Knee_rot_max, Ankle_flex_max, Ankle_add_max, Ankle_rot_max);

% Names = {'Error direction', 'Error mm', 'Hip Flex RMSD', 'Hip Flex SD', 'Hip Add RMSD', 'Hip Add SD', 'Hip Rot RMSD', 'Hip Rot SD', 'Knee Flex RMSD', 'Knee Flex SD', 'Knee Add RMSD', 'Knee Add SD', ...
% 'Knee Rot RMSD', 'Knee Rot SD', 'Ankle Flex RMSD', 'Ankle Flex SD', 'Ankle Add RMSD', 'Ankle Add SD', 'Ankle Rot RMSD', 'Ankle Rot SD'};

T_m_RMSD = table(Err_cmp',Magn',m_RMSD(:,1),m_std(:,1),m_RMSD(:,2),m_std(:,2),m_RMSD(:,3),m_std(:,3),m_RMSD(:,4),m_std(:,4),m_RMSD(:,5),m_std(:,5),m_RMSD(:,6),m_std(:,6),m_RMSD(:,7),m_std(:,7),m_RMSD(:,8),m_std(:,8), m_RMSD(:,9),m_std(:,9)); %, 'VariableNames', Names);
T_m_max  = table(Err_cmp',Magn',m_max_RMSD);

count = 0;
m_RMSD_pMagn = zeros (size(m_RMSD));
for j = 1:length(Er)
    c = j;
    for i=1:length(Error)
       count = count+1;
       m_RMSD_pMagn(count,:) = m_RMSD(c,:);
       c = c +5;
    end
end    
T_m_RMSD_pMagn = table(Err_cmp',Magn', m_RMSD_pMagn) ;

end