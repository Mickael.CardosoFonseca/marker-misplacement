function [T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_norm(Angles, Error, Er, MM, counter, C3D_filename, data_path)
    
%% 1. Normalize gait cycle
    acq = btkReadAcquisition(strcat(data_path, C3D_filename));
    ff = btkGetFirstFrame(acq);
    events = btkGetEvents(acq);
    event_ff = (round(events.Left_Foot_Strike(1)*100))-ff+1;
    event_lf = round(events.Left_Foot_Strike(2)*100)-ff+1;
    time = 0:100;
    
    
    direction   = {'Ant', 'Ant_Prox', 'Prox', 'Post_Prox', 'Post', 'Post_Dist', 'Dist', 'Ant_Dist'};
for ii = 1:counter
    RMSD_hipflex   = [];
    RMSD_hipadd    = [];
    RMSD_hiprot    = [];
    RMSD_kneeflex  = [];
    RMSD_kneeadd   = [];
    RMSD_kneerot   = [];
    RMSD_ankleflex = [];
    RMSD_ankleadd  = [];
    RMSD_anklerot  = [];
    %% 2. Original angles
    L_Hip = Angles(ii).original.LHipAngles_PyCGM1(event_ff: event_lf, :);
    L_Knee = Angles(ii).original.LKneeAngles_PyCGM1(event_ff: event_lf, :);
    L_Ankle = Angles(ii).original.LAnkleAngles_PyCGM1(event_ff: event_lf, :);
    %% 3. Calculate RMSD
    for j = 1:length(Error)
        for k = 1:length(Er)
            EE = ['Misp_',num2str(Er(k))];
            Hip   = strcat('LHipAngles_', string(MM(k,j)));
            Knee  = strcat('LKneeAngles_', string(MM(k,j)));
            Ankle = strcat('LAnkleAngles_', string(MM(k,j)));
            
            L_Hip_err = Angles(ii).error.(char(Hip))(event_ff: event_lf, :);
            L_Knee_err = Angles(ii).error.(char(Knee))(event_ff: event_lf, :);
            L_Ankle_err = Angles(ii).error.(char(Ankle))(event_ff: event_lf, :);
            
            % Calculate RMSD for each angle and each error
            RMSD_hipflex(:,j)   =  sqrt((L_Hip_err(:,1) - L_Hip(:,1)).^2);
            RMSD_hipadd(:,j)    =  sqrt((L_Hip_err(:,2) - L_Hip(:,2)).^2);
            RMSD_hiprot(:,j)    =  sqrt((L_Hip_err(:,3) - L_Hip(:,3)).^2);
            RMSD_kneeflex(:,j)  =  sqrt((L_Knee_err(:,1) - L_Knee(:,1)).^2);
            RMSD_kneeadd(:,j)   =  sqrt((L_Knee_err(:,2) - L_Knee(:,2)).^2);
            RMSD_kneerot(:,j)   =  sqrt((L_Knee_err(:,3) - L_Knee(:,3)).^2);
            RMSD_ankleflex(:,j) =  sqrt((L_Ankle_err(:,1) - L_Ankle(:,1)).^2);
            RMSD_ankleadd(:,j)  =  sqrt((L_Ankle_err(:,2) - L_Ankle(:,2)).^2);
            RMSD_anklerot(:,j)  =  sqrt((L_Ankle_err(:,3) - L_Ankle(:,3)).^2);         
            
            % RMSD
            R.LHip_flex.(direction{j}).(EE).RMSD(ii)   =  mean(RMSD_hipflex(:,j));
            R.LHip_add.(direction{j}).(EE).RMSD(ii)    =  mean(RMSD_hipadd(:,j));
            R.LHip_rot.(direction{j}).(EE).RMSD(ii)    =  mean(RMSD_hiprot(:,j));
            R.LKnee_flex.(direction{j}).(EE).RMSD(ii)  =  mean(RMSD_kneeflex(:,j));
            R.LKnee_add.(direction{j}).(EE).RMSD(ii)   =  mean(RMSD_kneeadd(:,j));
            R.LKnee_rot.(direction{j}).(EE).RMSD(ii)   =  mean(RMSD_kneerot(:,j));
            R.LAnkle_flex.(direction{j}).(EE).RMSD(ii) =  mean(RMSD_ankleflex(:,j));
            R.LAnkle_add.(direction{j}).(EE).RMSD(ii)  =  mean(RMSD_ankleadd(:,j));
            R.LAnkle_rot.(direction{j}).(EE).RMSD(ii)  =  mean(RMSD_anklerot(:,j));
            
            % max RMSD
            R.LHip_flex.(direction{j}).(EE).max_RMSD(ii)   =  max(RMSD_hipflex(:,j));
            R.LHip_add.(direction{j}).(EE).max_RMSD(ii)    =  max(RMSD_hipadd(:,j));
            R.LHip_rot.(direction{j}).(EE).max_RMSD(ii)    =  max(RMSD_hiprot(:,j));
            R.LKnee_flex.(direction{j}).(EE).max_RMSD(ii)  =  max(RMSD_kneeflex(:,j));
            R.LKnee_add.(direction{j}).(EE).max_RMSD(ii)   =  max(RMSD_kneeadd(:,j));
            R.LKnee_rot.(direction{j}).(EE).max_RMSD(ii)   =  max(RMSD_kneerot(:,j));
            R.LAnkle_flex.(direction{j}).(EE).max_RMSD(ii) =  max(RMSD_ankleflex(:,j));
            R.LAnkle_add.(direction{j}).(EE).max_RMSD(ii)  =  max(RMSD_ankleadd(:,j));
            R.LAnkle_rot.(direction{j}).(EE).max_RMSD(ii)  =  max(RMSD_anklerot(:,j));
            
            % standard deviation RMSD
            R.LHip_flex.(direction{j}).(EE).SD(ii)   =  std(RMSD_hipflex(:,j));
            R.LHip_add.(direction{j}).(EE).SD(ii)    =  std(RMSD_hipadd(:,j));
            R.LHip_rot.(direction{j}).(EE).SD(ii)    =  std(RMSD_hiprot(:,j));
            R.LKnee_flex.(direction{j}).(EE).SD(ii)  =  std(RMSD_kneeflex(:,j));
            R.LKnee_add.(direction{j}).(EE).SD(ii)   =  std(RMSD_kneeadd(:,j));
            R.LKnee_rot.(direction{j}).(EE).SD(ii)   =  std(RMSD_kneerot(:,j));
            R.LAnkle_flex.(direction{j}).(EE).SD(ii) =  std(RMSD_ankleflex(:,j));
            R.LAnkle_add.(direction{j}).(EE).SD(ii)  =  std(RMSD_ankleadd(:,j));
            R.LAnkle_rot.(direction{j}).(EE).SD(ii)  =  std(RMSD_anklerot(:,j));
            
            
        end
    end
end


end