function [mean, SD] = Mean_Gait_Cycles3(C3D_filenames, C3D_path, static_file)
% Plot kinematics for one patient: mean+/-SD and misplacement
cd 'D:\Marker Misplacement Simulation\Test\Patient_1\'
%% 1. Original gait kinematics
% Calculate mean +/- SD for one session 
% Include all dynamic trials
figure(4)
for i = 1:length(C3D_filenames)
    acq = btkReadAcquisition(char(strcat(C3D_path, C3D_filenames{i})));
    Angles = btkGetAngles(acq);
    Events = btkGetEvents(acq);
    ff = btkGetFirstFrame(acq);
    HS_1 = (round(Events.Left_Foot_Strike(1)*100))-ff+1;
    HS_2 = (round(Events.Left_Foot_Strike(2)*100))-ff+1;
    HS_3 = (round(Events.Left_Foot_Strike(3)*100))-ff+1;
%     time = 0:100;
    x = 1:size(Angles.LHipAngles_PyCGM1(HS_1:HS_2,1),1);
    Hip_FE(:,i) = interp1(x',Angles.LHipAngles_PyCGM1(HS_1:HS_2,1), linspace(1,length(x),101));
    Hip_AA(:,i) = interp1(x',Angles.LHipAngles_PyCGM1(HS_1:HS_2,2), linspace(1,length(x),101));
    Hip_R(:,i) = interp1(x',Angles.LHipAngles_PyCGM1(HS_1:HS_2,3), linspace(1,length(x),101));
    
    Knee_FE(:,i) = interp1(x',Angles.LKneeAngles_PyCGM1(HS_1:HS_2,1), linspace(1,length(x),101));
    Knee_AA(:,i) = interp1(x',Angles.LKneeAngles_PyCGM1(HS_1:HS_2,2), linspace(1,length(x),101));
    Knee_R(:,i) = interp1(x',Angles.LKneeAngles_PyCGM1(HS_1:HS_2,3), linspace(1,length(x),101));
    
    Ankle_FE(:,i) = interp1(x',Angles.LAnkleAngles_PyCGM1(HS_1:HS_2,1), linspace(1,length(x),101));
    Ankle_AA(:,i) = interp1(x',Angles.LAnkleAngles_PyCGM1(HS_1:HS_2,2), linspace(1,length(x),101));
    Ankle_R(:,i) = interp1(x',Angles.LAnkleAngles_PyCGM1(HS_1:HS_2,3), linspace(1,length(x),101));
    
    xx = 1:size(Angles.LHipAngles_PyCGM1(HS_2:HS_3,1),1);
    Hip_FE(:,i+length(C3D_filenames)) = interp1(xx',Angles.LHipAngles_PyCGM1(HS_2:HS_3,1), linspace(1,length(xx),101));
    Hip_AA(:,i+length(C3D_filenames)) = interp1(xx',Angles.LHipAngles_PyCGM1(HS_2:HS_3,2), linspace(1,length(xx),101));
    Hip_R(:,i+length(C3D_filenames)) = interp1(xx',Angles.LHipAngles_PyCGM1(HS_2:HS_3,3), linspace(1,length(xx),101));
    
    Knee_FE(:,i+length(C3D_filenames)) = interp1(xx',Angles.LKneeAngles_PyCGM1(HS_2:HS_3,1), linspace(1,length(xx),101));
    Knee_AA(:,i+length(C3D_filenames)) = interp1(xx',Angles.LKneeAngles_PyCGM1(HS_2:HS_3,2), linspace(1,length(xx),101));
    Knee_R(:,i+length(C3D_filenames)) = interp1(xx',Angles.LKneeAngles_PyCGM1(HS_2:HS_3,3), linspace(1,length(xx),101));
    
    Ankle_FE(:,i+length(C3D_filenames)) = interp1(xx',Angles.LAnkleAngles_PyCGM1(HS_2:HS_3,1), linspace(1,length(xx),101));
    Ankle_AA(:,i+length(C3D_filenames)) = interp1(xx',Angles.LAnkleAngles_PyCGM1(HS_2:HS_3,2), linspace(1,length(xx),101));
    Ankle_R(:,i+length(C3D_filenames)) = interp1(xx',Angles.LAnkleAngles_PyCGM1(HS_2:HS_3,3), linspace(1,length(xx),101));  
end
    % calculate mean and std for each joint angles
    m_Hip_FE = mean(Hip_FE,2);
    sd_Hip_FE = std(Hip_FE,1,2);
    m_Hip_AA = mean(Hip_AA,2);
    sd_Hip_AA = std(Hip_AA,1,2);
    m_Hip_R = mean(Hip_R,2);
    sd_Hip_R = std(Hip_R,1,2);
    m_Knee_FE = mean(Knee_FE,2);
    sd_Knee_FE = std(Knee_FE,1,2);
    m_Knee_AA = mean(Knee_AA,2);
    sd_Knee_AA = std(Knee_AA,1,2);
    m_Knee_R = mean(Knee_R,2);
    sd_Knee_R = std(Knee_R,1,2);
    m_Ankle_FE = mean(Ankle_FE,2);
    sd_Ankle_FE = std(Ankle_FE,1,2);
    m_Ankle_AA = mean(Ankle_AA,2);
    sd_Ankle_AA = std(Ankle_AA,1,2);
    m_Ankle_R = mean(Ankle_R,2);
    sd_Ankle_R = std(Ankle_R,1,2);

%% 2. Misplacement computation on dynamic files - To do once
% % rs with Defined error - Define before computation
% % MAR1: marker to misplace.
% % SEGMENT: segment where the marker is used to define the LCS.
% % Er: error magnitudes in mm.
% % Error_dir: 'AP_ML' antero-posteior + medial lateral (e.g. LKNE, LANK);'AP_DP' antero-posteior + proximal distal; 'ML_PD' medial lateral + proximal distal (e.g Lasi);
% % seg_origin: segment origin
MARK1 = ('LKNE');
Magnitude = [5, 10, 15, 20, 30];
Error_dir = 'AP';
% 
% SEGMENT.name = ('LFEMUR');
% SEGMENT.origin = ('LHJC');
% SEGMENT.proximal = [SEGMENT.name, '_Z'];
% SEGMENT.lateral  = [SEGMENT.name, '_Y'];
% SEGMENT.anterior = [SEGMENT.name, '_X'];
% b=1;
angle = [0, 180];
% data_path = C3D_path;
% 
% %Run PyCGM
% commandStr1 = ['cd /d ' data_path];
% commandStr2 = ['python.exe ' 'pyCGM2_CGM11_modelling.py'];
% [status, commandOut] = system([commandStr1 ' & ' commandStr2],'-echo');
% delete('CGM1.1 [0].completeSettings')
% 
% %Simulate marker misplacement
% for mag = 1:length(Magnitude)
%    E = Magnitude(mag);
%    for ang = 1:length(angle)
%        if angle(ang)== 0 
%            Error = [E*cos(0), 0, 0]
%        elseif angle(ang) == 180
%            Error = [E*(-cos(0)), 0, 0]
%        end
%        %% Static file
%        % 2.1. Store original marker coordinates of static file
%        acq=btkReadAcquisition(strcat(data_path, static_file));
%        data_static=btkGetMarkers(acq);
%        MARK1_original_static = data_static.(MARK1);
%        btkWriteAcquisition(acq, static_file);
%        
%        % 2.2. Calculate LCS and add error to Marker in static file
%        MARK1_GCS     =  data_static.(MARK1);
%        MARK1_LCS     =  zeros(size(data_static.(SEGMENT.origin),1),4);
%        MARK1_Mis_LCS =  zeros(size(data_static.(SEGMENT.origin),1),3);
%        MARKER_MIS    =  [MARK1,'_', num2str(angle(ang)),'_', num2str(E), '_', Error_dir];
%        data_static.(MARKER_MIS)  =  zeros(size(data_static.(SEGMENT.origin),1),4);
%        marker.(MARKER_MIS) = [];
%        
%        for m = 1:size(data_static.(SEGMENT.origin),1)
%            % Rotation matrix from GCS to LCS (3x3)
%            mat_left_fem(:,:,m) = Femur_Mat_Rot(data_static.(SEGMENT.origin)(m,:),data_static.(SEGMENT.proximal)(m,:),data_static.(SEGMENT.lateral)(m,:),data_static.(SEGMENT.anterior)(m,:) );
%            % Transformation matrix (4x4)
%            T = [mat_left_fem(:,1,m), mat_left_fem(:,2,m), mat_left_fem(:,3,m), (data_static.(SEGMENT.origin)(m,:))'; 0, 0, 0, 1];
%            Transf = Tinv_array3(T);
%            % Calculate marker coordinates in LCS
%            MARK1_LCS(m,:) = (Transf*[MARK1_GCS(m,:)'; 1])';
%            % Add an error on the marker in LCS
%            MARK1_Mis_LCS(m,:) = [MARK1_LCS(m,1)+Error(1,1), MARK1_LCS(m,2)+Error(1,2), MARK1_LCS(m,3)+Error(1,3)];
%            % Calculate marker coordinates in GCS
%            data_static.(MARKER_MIS)(m,:) = (inv(Transf)*[MARK1_Mis_LCS(m,:),1]')';
%        end
%        data_static.(MARKER_MIS)(:,4) = [];
%        
%        % Update static file
%        to_delete = {'LKNE', 'LHJC', 'RHJC', 'LKJC', 'RKJC', 'LAJC','RAJC', 'LFEMUR_X', 'LFEMUR_Y', 'LFEMUR_Z', 'PELVIS_X','PELVIS_Y','PELVIS_Z', 'LTIBIA_X','LTIBIA_Y','LTIBIA_Z' };
%        acq=btkReadAcquisition(strcat(data_path, static_file));
%        for d = 1:length(to_delete)
%            btkRemovePoint(acq,to_delete{d});
%        end
%        % Replace the MARK1 by the modified one
%        btkAppendPoint(acq,'marker','LKNE',data_static.(MARKER_MIS));
%        btkWriteAcquisition(acq, static_file);
%        
%        %% Dynamic files
%        for i = 1:length(C3D_filenames)
%            % 2.3. Store original marker coordinates of dynamic files
%            dynamic_file = C3D_filenames{i};
%            acq  =  btkReadAcquisition(strcat(data_path, dynamic_file));
%            data_dynamic  =  btkGetMarkers(acq);
%            MARK1_original_dynamic(i).LKNE = data_dynamic.(MARK1);
%            btkWriteAcquisition(acq, dynamic_file);
%            
%            % 2.4. Compute misplacement for the dynamic files
%            MARK1_GCS     =  data_dynamic.(MARK1);
%            MARK1_LCS     =  zeros(size(data_dynamic.(SEGMENT.origin),1),4);
%            MARK1_Mis_LCS =  zeros(size(data_dynamic.(SEGMENT.origin),1),3);
%            data_dynamic.(MARKER_MIS)  =  zeros(size(data_dynamic.(SEGMENT.origin),1),4);
%            marker.(MARKER_MIS)  =  [];
%            
%            for m = 1:size(data_dynamic.(SEGMENT.origin),1)
%                mat_left_fem(:,:,m)  =  Femur_Mat_Rot(data_dynamic.(SEGMENT.origin)(m,:),data_dynamic.(SEGMENT.proximal)(m,:),data_dynamic.(SEGMENT.lateral)(m,:),data_dynamic.(SEGMENT.anterior)(m,:) );
%                T = [mat_left_fem(:,1,m), mat_left_fem(:,2,m), mat_left_fem(:,3,m), (data_dynamic.(SEGMENT.origin)(m,:))'; 0, 0, 0, 1];
%                Transf = Tinv_array3(T);
%                MARK1_LCS(m,:)  =  (Transf*[MARK1_GCS(m,:)'; 1])';
%                MARK1_Mis_LCS(m,:) =  [MARK1_LCS(m,1)+Error(1,1), MARK1_LCS(m,2)+Error(1,2), MARK1_LCS(m,3)+Error(1,3)];
%                data_dynamic.(MARKER_MIS)(m,:)  =  (inv(Transf)*[MARK1_Mis_LCS(m,:),1]')';
%            end
%            data_dynamic.(MARKER_MIS)(:,4)=[];
%            
%            % Update c3d file
%            C3D_d.acq=btkReadAcquisition(strcat(data_path,dynamic_file));
%            for d = 1:length(to_delete)
%                btkRemovePoint(C3D_d.acq,to_delete{d});
%            end
%            btkAppendPoint(C3D_d.acq,'marker','LKNE',data_dynamic.(MARKER_MIS)); % replace the MARK1 by the modified one
%            btkWriteAcquisition(C3D_d.acq, dynamic_file);
%            
%        end
%        
%        %% 2.5. Update suffix on CGM1_1.UserSettings file
%        fid = fopen('CGM1_1.userSettings','r');
%        bb = 1;
%        tline = fgetl(fid);
%        AA{bb} = tline;
%        while ischar(tline)
%            bb = bb+1;
%            tline = fgetl(fid);
%            AA{bb} = tline;
%        end
%        fclose(fid);
%        change = (['    Point suffix: ',strcat(MARKER_MIS)]); % Change cell AA
%        AA{36} = strjoin(cellstr(change));
%        
%        % Write cell AA into txt
%        fid = fopen('CGM1_1.userSettings', 'w');
%        for t = 1:numel(AA)
%            if AA{t+1} == -1
%                fprintf(fid,'%s', AA{t});
%                break
%            else
%                fprintf(fid,'%s\n', AA{t});
%            end
%        end
%        fclose(fid);
%        
%        %% 2.6. Run PyCGM2
%        commandStr1 = ['cd /d ' data_path];
%        commandStr2 = ['python.exe ' 'pyCGM2_CGM11_modelling.py'];
%        [status, commandOut] = system([commandStr1 ' & ' commandStr2],'-echo');
%        disp(['----------------------------------------------']);
%        disp(['End of computation for marker:', strcat(MARKER_MIS)]);
%        disp(['----------------------------------------------']);
%        
%        delete('CGM1.1 [0].completeSettings')
%        
%        %% 2.7. Restore c3d files
%        % static 
%        acq = btkReadAcquisition(strcat(data_path,char(static_file)));
%        M = btkGetMarkers(acq);
%        btkRemovePoint(acq, 'LKNE');
%        btkAppendPoint(acq, 'marker','LKNE',MARK1_original_static)
%        btkWriteAcquisition(acq, static_file);
%        
%        for list = 1:length(C3D_filenames)
%            acq = btkReadAcquisition(strcat(data_path,char(C3D_filenames{list})));
%            M = btkGetMarkers(acq);
%            btkRemovePoint(acq, 'LKNE');
%            btkAppendPoint(acq, 'marker','LKNE',MARK1_original_dynamic(list).LKNE)
%            btkWriteAcquisition(acq, C3D_filenames{list});
% 
%        end
%    end
% end

%% 3. MEAN error angles
angles = {0, 180};
AA = {'Anterior', 'Posterior'};
Magnitude = {5,10,15,20,30};
MAG = {'Mag_5', 'Mag_10', 'Mag_15','Mag_20', 'Mag_30'};
for i = 1:length(C3D_filenames)
    acq = btkReadAcquisition(strcat(data_path,char(C3D_filenames{i})));
    Angles = btkGetAngles(acq);
    Events = btkGetEvents(acq);
    ff = btkGetFirstFrame(acq);
    HS_1 = (round(Events.Left_Foot_Strike(1)*100))-ff+1;
    HS_2 = (round(Events.Left_Foot_Strike(2)*100))-ff+1;
    HS_3 = (round(Events.Left_Foot_Strike(3)*100))-ff+1;
    x = 1:size(Angles.LHipAngles_PyCGM1(HS_1:HS_2,1),1);
    xx = 1:size(Angles.LHipAngles_PyCGM1(HS_2:HS_3,1),1);        
    for ang = 1:2
       for mag = 1:5
          Kinematics.LHip_flex.(AA{ang}).(MAG{mag})(:,i) = (interp1(x', Angles.(char(strcat('LHipAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_1:HS_2,1), linspace(1,length(x),101)))';
          Kinematics.LHip_add.(AA{ang}).(MAG{mag})(:,i) = (interp1(x', Angles.(char(strcat('LHipAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_1:HS_2,2), linspace(1,length(x),101)))';
          Kinematics.LHip_rot.(AA{ang}).(MAG{mag})(:,i) = (interp1(x', Angles.(char(strcat('LHipAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_1:HS_2,3), linspace(1,length(x),101)))';
          
          Kinematics.LKnee_flex.(AA{ang}).(MAG{mag})(:,i) = (interp1(x', Angles.(char(strcat('LKneeAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_1:HS_2,1), linspace(1,length(x),101)))';
          Kinematics.LKnee_add.(AA{ang}).(MAG{mag})(:,i) = (interp1(x', Angles.(char(strcat('LKneeAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_1:HS_2,2), linspace(1,length(x),101)))';
          Kinematics.LKnee_rot.(AA{ang}).(MAG{mag})(:,i) = (interp1(x', Angles.(char(strcat('LKneeAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_1:HS_2,3), linspace(1,length(x),101)))';
          
          Kinematics.LAnkle_flex.(AA{ang}).(MAG{mag})(:,i) = (interp1(x', Angles.(char(strcat('LAnkleAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_1:HS_2,1), linspace(1,length(x),101)))';
          Kinematics.LAnkle_add.(AA{ang}).(MAG{mag})(:,i) = (interp1(x', Angles.(char(strcat('LAnkleAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_1:HS_2,2), linspace(1,length(x),101)))';
          Kinematics.LAnkle_rot.(AA{ang}).(MAG{mag})(:,i) = (interp1(x', Angles.(char(strcat('LAnkleAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_1:HS_2,3), linspace(1,length(x),101)))';
       
          Kinematics.LHip_flex.(AA{ang}).(MAG{mag})(:,i+length(C3D_filenames)) = (interp1(xx', Angles.(char(strcat('LHipAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_2:HS_3,1), linspace(1,length(xx),101)))';
          Kinematics.LHip_add.(AA{ang}).(MAG{mag})(:,i+length(C3D_filenames)) = (interp1(xx', Angles.(char(strcat('LHipAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_2:HS_3,2), linspace(1,length(xx),101)))';
          Kinematics.LHip_rot.(AA{ang}).(MAG{mag})(:,i+length(C3D_filenames)) = (interp1(xx', Angles.(char(strcat('LHipAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_2:HS_3,3), linspace(1,length(xx),101)))';
          
          Kinematics.LKnee_flex.(AA{ang}).(MAG{mag})(:,i+length(C3D_filenames)) = (interp1(xx', Angles.(char(strcat('LKneeAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_2:HS_3,1), linspace(1,length(xx),101)))';
          Kinematics.LKnee_add.(AA{ang}).(MAG{mag})(:,i+length(C3D_filenames)) = (interp1(xx', Angles.(char(strcat('LKneeAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_2:HS_3,2), linspace(1,length(xx),101)))';
          Kinematics.LKnee_rot.(AA{ang}).(MAG{mag})(:,i+length(C3D_filenames)) = (interp1(xx', Angles.(char(strcat('LKneeAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_2:HS_3,3), linspace(1,length(xx),101)))';
          
          Kinematics.LAnkle_flex.(AA{ang}).(MAG{mag})(:,i+length(C3D_filenames)) = (interp1(xx', Angles.(char(strcat('LAnkleAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_2:HS_3,1), linspace(1,length(xx),101)))';
          Kinematics.LAnkle_add.(AA{ang}).(MAG{mag})(:,i+length(C3D_filenames)) = (interp1(xx', Angles.(char(strcat('LAnkleAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_2:HS_3,2), linspace(1,length(xx),101)))';
          Kinematics.LAnkle_rot.(AA{ang}).(MAG{mag})(:,i+length(C3D_filenames)) = (interp1(xx', Angles.(char(strcat('LAnkleAngles_LKNE_',string(angles(ang)),'_',string(Magnitude(mag)),'_AP')))(HS_2:HS_3,3), linspace(1,length(xx),101)))';
       
       end
    end
end
    %% Plot results
    
figure(10)
PlotError_for_one_patient(m_Hip_FE, sd_Hip_FE, Kinematics.LHip_flex.Anterior, Kinematics.LHip_flex.Posterior, -30, 80, 'Hip Flexion-Extension', MAG, 1)
PlotError_for_one_patient(m_Hip_AA, sd_Hip_AA, Kinematics.LHip_add.Anterior, Kinematics.LHip_add.Posterior, -20, 20, 'Hip Adduction-Abduction', MAG, 2)
PlotError_for_one_patient(m_Hip_R, sd_Hip_R, Kinematics.LHip_rot.Anterior, Kinematics.LHip_rot.Posterior, -20, 20, 'Hip Internal-External Rotation', MAG, 3)
PlotError_for_one_patient(m_Knee_FE, sd_Knee_FE, Kinematics.LKnee_flex.Anterior, Kinematics.LKnee_flex.Posterior, -30, 80, 'Knee Flexion-Extension', MAG, 4)
PlotError_for_one_patient(m_Knee_AA, sd_Knee_AA, Kinematics.LKnee_add.Anterior, Kinematics.LKnee_add.Posterior, -20, 20, 'Knee Adduction-Abduction', MAG, 5)
PlotError_for_one_patient(m_Knee_R, sd_Knee_R, Kinematics.LKnee_rot.Anterior, Kinematics.LKnee_rot.Posterior, -20, 20, 'Knee Internal-External Rotation', MAG, 6)
PlotError_for_one_patient(m_Ankle_FE, sd_Ankle_FE, Kinematics.LAnkle_flex.Anterior, Kinematics.LAnkle_flex.Posterior, -30, 80, 'Ankle Flexion-Extension', MAG, 7)
PlotError_for_one_patient(m_Ankle_AA, sd_Ankle_AA, Kinematics.LAnkle_add.Anterior, Kinematics.LAnkle_add.Posterior, -20, 20, 'Ankle Adduction-Abduction', MAG, 8)
PlotError_for_one_patient(m_Ankle_R, sd_Ankle_R, Kinematics.LAnkle_rot.Anterior, Kinematics.LAnkle_rot.Posterior, -20, 20, 'Ankle Internal-External Rotation', MAG,9)


end