% Create a misplacement of a specific marker and recompute kinematic data
% with PyCGM2
% Author: MFonseca # April 2019
close all
clc

%% 1. Create virtual marke
% rs with Defined error - Define before computation
% MAR1: marker to misplace.
% SEGMENT: segment where the marker is used to define the LCS.
% Er: error magnitudes in mm.
% Error_dir: 'AP_ML' antero-posteior + medial lateral (e.g. LKNE, LANK);'AP_DP' antero-posteior + proximal distal; 'ML_PD' medial lateral + proximal distal (e.g Lasi);
% seg_origin: segment origin
tic
MARK1 = ('LKNE');
SEGMENT = ('LFEMUR');
Er = [5, 10, 15, 20, 30];
Error_dir = 'AP_ML';
seg_origin ='LHJC';
seg_prox=[SEGMENT, '_Z'];
seg_lat=[SEGMENT, '_Y'];
seg_ant=[SEGMENT, '_X'];
 k = 22;
b=1;
angle = [0; 45; 90; 135; 180; 225; 270; 315];

%% 2. Select files
original_datapath = 'D:\Marker Misplacement Simulation\Test\Original_Data\';
data_path  = 'D:\Marker Misplacement Simulation\Test\Test_2\';
patients   = dir([data_path,'*.c3d']);
[C3D_filenames, C3D_path, FilterIndex]=uigetfile({'*.C3D'},'S�lectionner les ficihers C3D ou GCD',['D:\Marker Misplacement Simulation\Test\Original_Data\' '/'],'MultiSelect','on');
cd 'D:\Marker Misplacement Simulation\Test\Original_Data\'

%% 3. Move and rename files
for i=1:length(C3D_filenames)
    copyfile(char(C3D_filenames{i}),data_path);
    C3D_filenames{i} = char(C3D_filenames{i});
end
    %% 5. Run PyGCM2 on renamed files
    cd 'D:\Marker Misplacement Simulation\Test\Test_2\'
    counter= 0;    
    dynamic_list = [];
for i = 1: length(C3D_filenames)
    patient_list = {}; %list of filenames relative to the static file
    if isempty(strfind(C3D_filenames{i},'SB'))==0
        %% 5.1 Find respective dynamic file
        static_file = C3D_filenames{i};
        patient_list = {static_file};
        c = 1;
        for j = 1:length(C3D_filenames)
            if isempty(strfind(C3D_filenames{i},C3D_filenames{j})) && isempty(strfind('G',C3D_filenames{j}(end-16)))==0
                if isempty(strfind(static_file(1,end-25:end-18),C3D_filenames{j}(1,end-25:end-18)))==0 && ~isempty(strfind(static_file(1,end-16:end-15),C3D_filenames{j}(1,end-16:end-15)))==0
%                     if isempty(strfind('G',C3D_filenames{j}(end-16)))==0
                        patient_list{end+1} = C3D_filenames{j};
                        dynamic_list = [dynamic_list; string(C3D_filenames{j})];
%                     end
                end
            end
        end   
        counter = counter +1;
        for s = 1:length(Er)         
            %% 4. DEFINE Error
            E = Er(s);
            if Error_dir == 'AP_DP'
                Error = [E*cos(0), E*sin(0) , 0; E*cos(45), E*sin(45), 0; 0, E, 0; -E*cos(45), E*sin(45), 0; -E, 0, 0; ...
                    -E*cos(45), -E*sin(45), 0; 0, -E, 0; E*cos(45), -E*cos(45), 0];
            elseif Error_dir == 'AP_ML'
                Error = [E, 0 , 0; E*sin(45), 0, E*cos(45); 0, 0, E; -E*sin(45),0, E*cos(45); -E, 0, 0; ...
                    -E*sin(45), 0, -E*cos(45); 0, 0, -E; E*sin(45), 0, -E*sin(45)];
            elseif Error_dir == 'ML_DP'
                Error = [0, E, 0; 0, E*cos(45), E*sin(45); 0, 0, E; 0, -E*cos(45), E*sin(45); 0, -E, 0; ...
                    0, -E*cos(45), -E*sin(45); 0, 0, -E; 0, E*cos(45), -E*sin(45)];
            else
                disp('The error direction is not valid.')
            end
            
            if s == 1
                %% 6 Run PyCGM2  % To add the joint centers and direction components for each segment (no need to be runned if already done)
                RunPyCGM2_ff(patient_list, data_path);
                
                %% 7.Save origin PyCGM angles
                dynamic_file = patient_list{2};
                acq = btkReadAcquisition(strcat(data_path, dynamic_file));
                Mark_ori = btkGetMarkers(acq);
                MARK1_original_dynamic = Mark_ori.(MARK1);
                Angles(counter).original = btkGetAngles(acq); 
                btkWriteAcquisition(acq, dynamic_file);
                
                acq = btkReadAcquisition(strcat(data_path, static_file));
                Mark_ori = btkGetMarkers(acq);
                MARK1_original_static = Mark_ori.(MARK1);
                Angles_static(counter).original = btkGetAngles(acq);
                btkWriteAcquisition(acq, static_file);
            end
            
            %% 8. Create marker with error
            for j=1:size(Error,1)
                %% 8.1 Get MARK1 in STATIC file
                acq=btkReadAcquisition(strcat(data_path, static_file));
                data_static=btkGetMarkers(acq);
                btkWriteAcquisition(acq, static_file);
                
                %% 8.2 calculate LCS and add error to MARK1 in STATIC
                MARK1_GCS     =  data_static.(MARK1);
                MARK1_LCS     =  zeros(size(data_static.(seg_origin),1),4);
                MARK1_Mis_LCS =  zeros(size(data_static.(seg_origin),1),3);
                MARKER_MIS    =  [MARK1,'_', num2str(angle(j)),'_', num2str(E), '_', Error_dir];
                MM{s,j}         =  MARKER_MIS;
                data_static.(MARKER_MIS)  =  zeros(size(data_static.(seg_origin),1),4);
                marker.(MARKER_MIS) = [];
           
                for m = 1:size(data_static.(seg_origin),1)
                    % Rotation matrix from GCS to LCS (3x3)
                    mat_left_fem(:,:,m) = Femur_Mat_Rot(data_static.(seg_origin)(m,:),data_static.(seg_prox)(m,:),data_static.(seg_lat)(m,:),data_static.(seg_ant)(m,:) );
                    % Transformation matrix (4x4)
                    T = [mat_left_fem(:,1,m), mat_left_fem(:,2,m), mat_left_fem(:,3,m), (data_static.(seg_origin)(m,:))'; 0, 0, 0, 1];
                    Transf = Tinv_array3(T);
                    % Calculate marker coordinates in LCS
                    MARK1_LCS(m,:) = (Transf*[MARK1_GCS(m,:)'; 1])';
                    % Add an error on the marker in LCS
                    MARK1_Mis_LCS(m,:) = [MARK1_LCS(m,1)+Error(j,1), MARK1_LCS(m,2)+Error(j,2), MARK1_LCS(m,3)+Error(j,3)];
                    % Calculate marker coordinates in GCS
                    data_static.(MARKER_MIS)(m,:) = (inv(Transf)*[MARK1_Mis_LCS(m,:),1]')';
                end
                data_static.(MARKER_MIS)(:,4) = [];

                %% 8.3 read C3D in DYNAMIC
                dynamic_file = patient_list{2};
                acq  =  btkReadAcquisition(strcat(data_path, dynamic_file));
                data_dynamic  =  btkGetMarkers(acq);
                btkWriteAcquisition(acq, dynamic_file);
                
                %% 8.4 calculate LCS and add error to MARK1 in Dynamic
                MARK1_GCS     =  data_dynamic.(MARK1);
                MARK1_LCS     =  zeros(size(data_dynamic.(seg_origin),1),4);
                MARK1_Mis_LCS =  zeros(size(data_dynamic.(seg_origin),1),3);
                data_dynamic.(MARKER_MIS)  =  zeros(size(data_dynamic.(seg_origin),1),4);
                marker.(MARKER_MIS)  =  [];

                for m = 1:size(data_dynamic.(seg_origin),1)
                    mat_left_fem(:,:,m)  =  Femur_Mat_Rot(data_dynamic.(seg_origin)(m,:),data_dynamic.(seg_prox)(m,:),data_dynamic.(seg_lat)(m,:),data_dynamic.(seg_ant)(m,:) );
                    T = [mat_left_fem(:,1,m), mat_left_fem(:,2,m), mat_left_fem(:,3,m), (data_dynamic.(seg_origin)(m,:))'; 0, 0, 0, 1];
                    Transf = Tinv_array3(T);
                    MARK1_LCS(m,:)  =  (Transf*[MARK1_GCS(m,:)'; 1])';
                    MARK1_Mis_LCS(m,:) =  [MARK1_LCS(m,1)+Error(j,1), MARK1_LCS(m,2)+Error(j,2), MARK1_LCS(m,3)+Error(j,3)];
                    data_dynamic.(MARKER_MIS)(m,:)  =  (inv(Transf)*[MARK1_Mis_LCS(m,:),1]')';
                end
                data_dynamic.(MARKER_MIS)(:,4)=[];

                %% 8.5 Delete joint centers and restore append virtual markers in the 2 files
                to_delete = {'LHJC', 'RHJC', 'LKJC', 'RKJC', 'LAJC','RAJC', 'LFEMUR_X', 'LFEMUR_Y', 'LFEMUR_Z', 'PELVIS_X','PELVIS_Y','PELVIS_Z', 'LTIBIA_X','LTIBIA_Y','LTIBIA_Z' };
                C3D_s.acq=btkReadAcquisition(strcat(data_path,static_file));
                for d = 1:length(to_delete)
                    btkRemovePoint(C3D_s.acq,to_delete{d});
                end
                btkAppendPoint(C3D_s.acq,'marker',char(MARKER_MIS),data_static.(MARKER_MIS));
                btkWriteAcquisition(C3D_s.acq, static_file);

                C3D_d.acq=btkReadAcquisition(strcat(data_path,dynamic_file));
                for d = 1:length(to_delete)
                    btkRemovePoint(C3D_d.acq,to_delete{d});
                end
                btkAppendPoint(C3D_d.acq,'marker',char(MARKER_MIS),data_dynamic.(MARKER_MIS));
                btkWriteAcquisition(C3D_d.acq, dynamic_file);
                %% 9. Update translators
                trans = {'LASI','RASI','LPSI','RPSI','RTHI','RKNE','RKNM','RTIB','RANK','RMED','RHEE','RTOE','LTHI','LKNE','LKNM','LTIB','LANK','LMED','LHEE','LTOE','C7',...
                    'T10','CLAV','STRN','LFHD','LBHD','RFHD','RBHD','LSHO','LELB','LWRB','LWRA','LFIN','RSHO','RELB','RWRB','RWRA','RFIN'};
                space= '    ';
                % Replace in translators, MARK1 = None for MARK1 = (virtual marker (m))
                for r = 1:length(trans)
                    if strfind(trans{r}, MARK1)
                        File.Translators.(trans{r})= MARKER_MIS;
                    else
                        File.Translators.(trans{r})='None';
                    end
                end
                file_name = [data_path, 'CGM1_1.translators'];
                fid = fopen(file_name,'w+');
                f = fieldnames(File);
                fprintf(fid, [f{1},':','\n']);
                ff= fieldnames(File.Translators);
                for k=1:size(ff,1)
                    fprintf(fid,[space,ff{k},': ',char(File.Translators.(ff{k})),'\n']);
                end
                %     Replace in user.Settings Point suffix = None by the name of the
                %     virtual marker to identify the new angles
                fid = fopen('CGM1_1.userSettings','r');
                if fid == -1
                    disp('Warning: CGM1_1.usersettings could not be open.')
                end
                i = 1;
                tline = fgetl(fid);
                AA{i} = tline;
                while ischar(tline)
                    i = i+1;
                    tline = fgetl(fid);
                    AA{i} = tline;
                end
                fclose(fid);   

                %% 10. Update suffix on CGM1_1.userSettings
                fid = fopen('CGM1_1.userSettings','r');
                bb = 1;
                tline = fgetl(fid);
                AA{bb} = tline;
                while ischar(tline)
                    bb = bb+1;
                    tline = fgetl(fid);
                    AA{bb} = tline;
                end
                fclose(fid);
                change = (['    Point suffix: ',strcat(MARKER_MIS)]); % Change cell AA
                AA{36} = strjoin(cellstr(change));

                % Write cell AA into txt
                fid = fopen('CGM1_1.userSettings', 'w');
                for t = 1:numel(AA)
                    if AA{t+1} == -1
                        fprintf(fid,'%s', AA{t});
                        break
                    else
                        fprintf(fid,'%s\n', AA{t});
                    end
                end
                fclose(fid);
                
                %% 
                %% 11. Run PyGCM2
                commandStr1 = ['cd /d ' data_path];
                commandStr2 = ['python.exe ' 'pyCGM2_CGM11_modelling.py'];
                [status, commandOut] = system([commandStr1 ' & ' commandStr2],'-echo');
                disp(['----------------------------------------------']);
                disp(['End of computation for marker:', strcat(MARKER_MIS)]);
                disp(['----------------------------------------------']);
                
                delete('CGM1.1 [0].completeSettings')
                
                %% 12. Restore MARK1 data (to avoid error)
                acq = btkReadAcquisition(strcat(data_path,char(static_file)));
                M = btkGetMarkers(acq);
                btkRemovePoint(acq, MARK1);
                btkAppendPoint(acq, 'marker',MARK1,MARK1_original_static)
                btkWriteAcquisition(acq, static_file);
                                
                acq = btkReadAcquisition(strcat(data_path,char(dynamic_file)));
                M = btkGetMarkers(acq);
                btkRemovePoint(acq, MARK1);
                btkAppendPoint(acq, 'marker',MARK1,MARK1_original_dynamic)
                btkWriteAcquisition(acq, dynamic_file);
            end
            %% 13. Store kinematic data
            acq = btkReadAcquisition(strcat(data_path,char(dynamic_file))); 
            Angles(counter).error = btkGetAngles(acq);
            btkWriteAcquisition(acq, dynamic_file);
            
            acq = btkReadAcquisition(strcat(data_path,char(static_file))); 
            Angles_static(counter).error = btkGetAngles(acq);
            btkWriteAcquisition(acq, static_file);
        end    
    end
end

%% 14. Calculate RMSD

% compute the results and export RMSD, std and max and mean(over patients)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_RMSD(Angles, Error, Er, MM, counter); 

% save Angles
% writetable(T_RMSD, 'RMSD_LKNE.xls')
% writetable(T_m_RMSD, 'mean_RMSD_LKNE.xls')
% writetable(T_m_max, 'mean_maxRMSD_LKNE.xls')
% writetable(T_m_RMSD_pMagn, 'm_RMSD_pMagn_LKNE.xls')
HipFlex_1 = vec2mat(m_RMSD_pMagn(:,1),length(Error));
HipFlex_1 = [HipFlex_1(:,1) fliplr(HipFlex_1(:,2:8))];
HipAdd_1  = vec2mat(m_RMSD_pMagn(:,2),length(Error));
HipAdd_1 = [HipAdd_1(:,1) fliplr(HipAdd_1(:,2:8))];
HipRot_1  = vec2mat(m_RMSD_pMagn(:,3),length(Error));
HipRot_1 = [HipRot_1(:,1) fliplr(HipRot_1(:,2:8))];

KneeFlex_1 = vec2mat(m_RMSD_pMagn(:,4),length(Error));
KneeFlex_1 = [KneeFlex_1(:,1) fliplr(KneeFlex_1(:,2:8))];
KneeAdd_1  = vec2mat(m_RMSD_pMagn(:,5),length(Error));
KneeAdd_1 = [KneeAdd_1(:,1) fliplr(KneeAdd_1(:,2:8))];
KneeRot_1  = vec2mat(m_RMSD_pMagn(:,6),length(Error));
KneeRot_1 = [KneeRot_1(:,1) fliplr(KneeRot_1(:,2:8))];

AnkleFlex_1 = vec2mat(m_RMSD_pMagn(:,7),length(Error));
AnkleFlex_1 = [AnkleFlex_1(:,1) fliplr(AnkleFlex_1(:,2:8))];
AnkleAdd_1  = vec2mat(m_RMSD_pMagn(:,8),length(Error));
AnkleAdd_1 = [AnkleAdd_1(:,1) fliplr(AnkleAdd_1(:,2:8))];
AnkleRot_1  = vec2mat(m_RMSD_pMagn(:,9),length(Error));
AnkleRot_1 = [AnkleRot_1(:,1) fliplr(AnkleRot_1(:,2:8))];

Lable = {'     Ant', 'Ant + Dist', 'Dist', 'Post + Dist', 'Post', 'Post + Prox', 'Prox', 'Ant + Prox'};
LineColor = {'b', 'c', 'g', 'm', 'r'};
LineStyle = {'no', ':'};
LevelNum = 5;
maximo = 17;


figure(1)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_norm(Angles, Error, Er, MM, dynamic_file, data_path, 'cycle')
MIS_PlotPolar(m_RMSD_pMagn, Error)

figure(20)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_norm(Angles, Error, Er, MM, dynamic_file, data_path, 'stance')
MIS_PlotPolar(m_RMSD_pMagn, Error)

figure(21)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_norm(Angles, Error, Er, MM, dynamic_file, data_path, 'swing')
MIS_PlotPolar(m_RMSD_pMagn, Error)


%% Table1.

[RR, Table1] = MIS_table_RMSD(Angles, Error, Er, MM, counter, data_path, C3D_filenames); 
writetable(Table1, 'Table Results RMSD.xls');
%% 15. Boxplot
% mis_bplot(Angles, Error, Er, MM, counter, neg_dir, pos_dir)

%% 16. Polar plot
figure(1)
MIS_PlotPolar(m_RMSD_pMagn, Error)
Lable = {'     Ant', 'Ant + Dist', 'Dist', 'Post + Dist', 'Post', 'Post + Prox', 'Prox', 'Ant + Prox'};
LineColor = {'b', 'c', 'g', 'm', 'r'};
LineStyle = {'no', ':'};
LevelNum = 5;
maximo = 17;

% Hip Flexion
figure (1)
MIS_Radar_Plot(HipFlex_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Hip Flexion / Extension')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(1), 'Hip Flexion.pdf')
% Hip Add/Abd
figure (2)
MIS_Radar_Plot(HipAdd_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Hip Ab/Adduction')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(2), 'Hip Add.pdf')
% Hip Rot
figure (3)
MIS_Radar_Plot(HipRot_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Hip Int/External Rotation')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
legend('5 mm', '10 mm', '15 mm', '20 mm', '30 mm')
saveas (figure(3), 'Hip Rot.pdf')
% knee Flexion
figure (4)
MIS_Radar_Plot(KneeFlex_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Knee Flex/Extension')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(4), 'Knee Flex.pdf')
% Knee Add/Abd
figure (5)
MIS_Radar_Plot(KneeAdd_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Knee Ab/Adduction')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(5), 'Knee Add.pdf')
% Knee Rot
figure (6)
MIS_Radar_Plot(KneeRot_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Knee Int/External Rotation')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(6), 'Knee Rot.pdf')
% Ankle Flexion
figure (7)
MIS_Radar_Plot(AnkleFlex_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Ankle Flex/Extension')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(7), 'Ankle Flex.pdf')
% Ankle Add
figure (8)
MIS_Radar_Plot(AnkleAdd_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Ankle Ab/Adduction')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(8), 'Ankle Add.pdf')
% Ankle Flexion
figure (9)
MIS_Radar_Plot(AnkleRot_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Ankle Int/External Rotation')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(9), 'Ankle Rot.pdf')


%% 17. Scatter plot (RMSD�/Error normalized)
 anthro = MIS_anthropometric_data(C3D_filenames, data_path);                 % get anthropometric data
 Leg_length = zeros(1, length(anthro.subject));
 Knee_Width = zeros(1, length(anthro.subject));
 for i = 1:length(anthro.subject)
     Leg_length(i) = str2double(anthro.subject(i).Left_LegLength_mm);
     Knee_Width(i) = str2double(anthro.subject(i).Left_KneeWidth_mm);
 end
 figure(2)
 hold on
 for i = 1:10
    [correl_LL, C, Table2] = MIS_Correlation(Leg_length(i), R, Er, 6, 30, 'Leg length', i);   % correl = correlation between  error and leg_length normalized; norm_errors:
 end
 
 
 correl = MIS_Correlation_all_pat(Leg_length(i), R, Er, 6, 30, 'Leg length')

 
 figure(3)
 [correl_KW, C, Table2] = MIS_Correlation(Knee_Width, R, Er, 40, 25, 'Knee Width');
 writetable(Table2, 'Table Correlation RMSD Leg Length.xls');

%% 18. Remove mean
figure (4)
C3D_filename = '10_00543_04508_20171107-GBNNN-VDEF-14.C3D';
R_Offset = MIS_RemoveOffset(Angles, Error, Er, MM, 10, data_path, C3D_filename)

figure (5)
C3D_filename7 = '07_02452_04602_20180403-GBNNN-VDEF-11.C3D';
R_Offset = MIS_RemoveOffset(Angles, Error, Er, MM, 7, data_path, C3D_filename7)

figure (6)
C3D_filename = '03_02595_04561_20180202-GBNNN-VDEF-09.C3D';
R_Offset = MIS_RemoveOffset(Angles, Error, Er, MM, 3, data_path, C3D_filename3)


%% 19. Plot Curves
subject = 9;
LineColor = {'b', 'c', 'g', 'm', 'r'};


% Plot Curves containing all misplacements * magnitudes
% Hip
figure(7)
MIS_PlotCurves(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, 'Hip')

% Knee
figure(8)
MIS_PlotCurves(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, 'Knee')

% Ankle
figure(9)
MIS_PlotCurves(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, 'Ankle')

% Plot Curves containing only one direction (e.g. ant + post) * magnitudes
Angle1 = 0;
Angle2 = 180;

figure(10)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Hip', 'Ant Post')

figure(11)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Knee', 'Ant Post')

figure(12)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Ankle', 'Ant Post')

%%
subject = 10;
Angle1 = 0;
Angle2 = 180;
figure (4)
MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Hip', 'Ant Post', 1)
MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Knee', 'Ant Post', 2)
MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Ankle', 'Ant Post', 3)
% MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'FootProgress', 'Ant Post', 3)



%%

Angle1 = 90;
Angle2 = 270;
figure(13)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Hip', 'Prox Dist')

figure(14)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Knee', 'Prox Dist')

figure(15)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Ankle', 'Prox Dist')

%% Save figures
saveas (figure(1), 'Polar Plot.pdf')
saveas (figure(2), 'Scatter RMSD/LegLength.pdf')
saveas (figure(3), 'Scatter RMSD/KneeWidth.pdf')
saveas (figure(4), 'Angle_Res_Pat10_AntPost.pdf')