function  R_Offset = MIS_RemoveOffset(Angles, Error, Er, MM, counter, data_path, C3D_filename)
    acq = btkReadAcquisition(strcat(data_path, C3D_filename));
    ff = btkGetFirstFrame(acq);
    events = btkGetEvents(acq);
    event_ff = (round(events.Left_Foot_Strike(1)*100))-ff+1;
    event_lf = round(events.Left_Foot_Strike(2)*100)-ff+1;
    
    time = 0:100;

direction   = {'Ant', 'Ant_Prox', 'Prox', 'Post_Prox', 'Post', 'Post_Dist', 'Dist', 'Ant_Dist'};
% for ii = 1:counter
ii = counter;
    RMSD_hipflex   = [];
    RMSD_hipadd    = [];
    RMSD_hiprot    = [];
    RMSD_kneeflex  = [];
    RMSD_kneeadd   = [];
    RMSD_kneerot   = [];
    RMSD_ankleflex = [];
    RMSD_ankleadd  = [];
    RMSD_anklerot  = [];
    
     %% 1. Original angles
    L_Hip   =  Angles(ii).original.LHipAngles_PyCGM1;
    L_Knee  =  Angles(ii).original.LKneeAngles_PyCGM1;
    L_Ankle =  Angles(ii).original.LAnkleAngles_PyCGM1;

     %% 2. Calculate RMSD
    for j = 1:length(Error)
        for k = 1:length(Er)
            EE    = ['Misp_',num2str(Er(k))];
            Hip   = strcat('LHipAngles_', string(MM(k,j)));
            Knee  = strcat('LKneeAngles_', string(MM(k,j)));
            Ankle = strcat('LAnkleAngles_', string(MM(k,j)));
            
            % Calculate offset
            L_Hip_err_offset   =  mean(Angles(ii).error.(char(Hip)))-mean(Angles(ii).original.LHipAngles_PyCGM1);
            L_Knee_err_offset  =  mean(Angles(ii).error.(char(Knee)))-mean(Angles(ii).original.LKneeAngles_PyCGM1);
            L_Ankle_err_offset =  mean(Angles(ii).error.(char(Ankle)))-mean(Angles(ii).original.LAnkleAngles_PyCGM1);
                       
            L_Hip_err   =  Angles(ii).error.(char(Hip));
            L_Knee_err  =  Angles(ii).error.(char(Knee));
            L_Ankle_err =  Angles(ii).error.(char(Ankle));
            
            % Calculate RMSD for each angle and each error
            RMSD_hipflex(:,j)   =  sqrt((L_Hip_err(:,1) - L_Hip(:,1)).^2);
            RMSD_hipadd(:,j)    =  sqrt((L_Hip_err(:,2) - L_Hip(:,2)).^2);
            RMSD_hiprot(:,j)    =  sqrt((L_Hip_err(:,3) - L_Hip(:,3)).^2);
            RMSD_kneeflex(:,j)  =  sqrt((L_Knee_err(:,1) - L_Knee(:,1)).^2);
            RMSD_kneeadd(:,j)   =  sqrt((L_Knee_err(:,2) - L_Knee(:,2)).^2);
            RMSD_kneerot(:,j)   =  sqrt((L_Knee_err(:,3) - L_Knee(:,3)).^2);
            RMSD_ankleflex(:,j) =  sqrt((L_Ankle_err(:,1) - L_Ankle(:,1)).^2);
            RMSD_ankleadd(:,j)  =  sqrt((L_Ankle_err(:,2) - L_Ankle(:,2)).^2);
            RMSD_anklerot(:,j)  =  sqrt((L_Ankle_err(:,3) - L_Ankle(:,3)).^2);
            
            % RMSD without the offset
            R_Offset.LHip_flex.(direction{j}).(EE).RMSD(ii)   =  mean(RMSD_hipflex(:,j))-abs(L_Hip_err_offset(1));
            R_Offset.LHip_add.(direction{j}).(EE).RMSD(ii)    =  mean(RMSD_hipadd(:,j))-abs(L_Hip_err_offset(2));
            R_Offset.LHip_rot.(direction{j}).(EE).RMSD(ii)    =  mean(RMSD_hiprot(:,j))-abs(L_Hip_err_offset(3));
            R_Offset.LKnee_flex.(direction{j}).(EE).RMSD(ii)  =  mean(RMSD_kneeflex(:,j))-abs(L_Knee_err_offset(1));
            R_Offset.LKnee_add.(direction{j}).(EE).RMSD(ii)   =  mean(RMSD_kneeadd(:,j))-abs(L_Knee_err_offset(2));
            R_Offset.LKnee_rot.(direction{j}).(EE).RMSD(ii)   =  mean(RMSD_kneerot(:,j))-abs(L_Knee_err_offset(3));
            R_Offset.LAnkle_flex.(direction{j}).(EE).RMSD(ii) =  mean(RMSD_ankleflex(:,j))-abs(L_Ankle_err_offset(1));
            R_Offset.LAnkle_add.(direction{j}).(EE).RMSD(ii)  =  mean(RMSD_ankleadd(:,j))-abs(L_Ankle_err_offset(2));
            R_Offset.LAnkle_rot.(direction{j}).(EE).RMSD(ii)  =  mean(RMSD_anklerot(:,j))-abs(L_Ankle_err_offset(3));


%             figure(11)
            subplot(3,3,1)
            plot(interp1(RMSD_hipflex(event_ff:event_lf,j)-abs(L_Hip_err_offset(1)), 1:101))
%             HipF = interp1(RMSD_hipflex(event_ff:event_lf,j)-abs(L_Hip_err_offset(1)), 1:101);
%             plot(HipF)
            xlim([0,100])
            ylim([-10 10])
            hold on
            
            subplot(3,3,2)
            plot(interp1(RMSD_hipadd(event_ff:event_lf,j)-abs(L_Hip_err_offset(2)), 1:101))
%             HipA = interp1(RMSD_hipadd(event_ff:event_lf,j))-abs(L_Hip_err_offset(2),1:101);
%             plot(HipA)
            xlim([0,100])
            ylim([-10 10])
            hold on
            
            subplot(3,3,3)
            plot(interp1(RMSD_hiprot(event_ff:event_lf,j)-abs(L_Hip_err_offset(3)), 1:101))
%             HipR = interp1(RMSD_hiprot(event_ff:event_lf,j)-abs(L_hip_err_offset(3)), 1:101);
%             plot(HipR)
            ylim([-10 10])
            xlim([0,100])
            hold on
            
            subplot(3,3,4)
            plot(interp1(RMSD_kneeflex(event_ff:event_lf,j)-abs(L_Knee_err_offset(1)), 1:101))
%             KneeF = interp1(RMSD_kneeflex(event_ff:event_lf,j)-abs(L_Knee_err_offset(1)), 1:101);
%             plot(KneeF)
            ylim([-10 10])
            xlim([0 100])
            hold on
            
            subplot(3,3,5)
            plot( interp1(RMSD_kneeadd(event_ff:event_lf,j)-abs(L_Knee_err_offset(2)), 1:101))
%             KneeA = interp1(RMSD_kneeadd(event_ff:event_lf,j)-abs(L_knee_err_offset(2)), 1:101);
%             plot(KneeA)
            ylim([-10 10])
            xlim([0 100])
            hold on
            
            subplot(3,3,6)
            plot(interp1(RMSD_kneerot(event_ff:event_lf,j)-abs(L_Knee_err_offset(3)), 1:101))
            ylim([-10 10])
            xlim([0 100])
            hold on
            
            subplot(3,3,7)
            plot(interp1(RMSD_ankleflex(event_ff:event_lf,j)-abs(L_Ankle_err_offset(1)), 1:101))
            ylim([-10 10])
            xlim([0 100])
            hold on
            
            subplot(3,3,8)
            plot(interp1(RMSD_ankleadd(event_ff:event_lf,j)-abs(L_Ankle_err_offset(2)), 1:101))
            ylim([-10 10])
            xlim([0 100])
            hold on
            
            subplot(3,3,9)
            plot(interp1(RMSD_anklerot(event_ff:event_lf,j)-abs(L_Ankle_err_offset(3)), 1:101))
            ylim([-10 10])
            xlim([0 100])
            hold on

            % ROM without the offset
            R_Offset.LHip_flex.(direction{j}).(EE).ROM_RMSD(ii)   =  max((RMSD_hipflex(:,j))-L_Hip_err_offset(1))-(min(RMSD_hipflex(:,j))-L_Hip_err_offset(1));
            R_Offset.LHip_add.(direction{j}).(EE).ROM_RMSD(ii)    =  max((RMSD_hipadd(:,j))-L_Hip_err_offset(2))-(min(RMSD_hipadd(:,j))-L_Hip_err_offset(2));
            R_Offset.LHip_rot.(direction{j}).(EE).ROM_RMSD(ii)    =  max((RMSD_hiprot(:,j))-L_Hip_err_offset(3))-(min(RMSD_hiprot(:,j))-L_Hip_err_offset(3));
            R_Offset.LKnee_flex.(direction{j}).(EE).ROM_RMSD(ii)  =  max((RMSD_kneeflex(:,j))-L_Hip_err_offset(1))-(min(RMSD_kneeflex(:,j))-L_Hip_err_offset(1));
            R_Offset.LKnee_add.(direction{j}).(EE).ROM_RMSD(ii)   =  max((RMSD_kneeadd(:,j))-L_Hip_err_offset(2))-(min(RMSD_kneeadd(:,j))-L_Hip_err_offset(2));
            R_Offset.LKnee_rot.(direction{j}).(EE).ROM_RMSD(ii)   =  max((RMSD_kneerot(:,j))-L_Hip_err_offset(3))-(min(RMSD_kneerot(:,j))-L_Hip_err_offset(3));
            R_Offset.LAnkle_flex.(direction{j}).(EE).ROM_RMSD(ii) =  max((RMSD_ankleflex(:,j))-L_Hip_err_offset(1))-(min(RMSD_ankleflex(:,j))-L_Hip_err_offset(1));
            R_Offset.LAnkle_add.(direction{j}).(EE).ROM_RMSD(ii)  =  max((RMSD_ankleadd(:,j))-L_Hip_err_offset(2))-(min(RMSD_ankleadd(:,j))-L_Hip_err_offset(2));
            R_Offset.LAnkle_rot.(direction{j}).(EE).ROM_RMSD(ii)  =  max((RMSD_anklerot(:,j))-L_Hip_err_offset(3))-(min(RMSD_anklerot(:,j))-L_Hip_err_offset(3));
            
        end
    end
end
