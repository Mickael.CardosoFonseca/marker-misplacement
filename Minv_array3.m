function Rinv = Minv_array3(R)

if round(det(R(:,:,1))) == 1 % Test sur la premi�re image
    Rinv = permute (R, [2,1,3]);
else
    for n = 1:size(R,3)
        Rinv(:,:,n) = inv(R(:,:,n)); % Inversion image par image
    end
end
