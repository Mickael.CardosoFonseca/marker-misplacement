function [Angles, MM, counter, Error] = MIS_Computation3(C3D_filenames, MARK_to_MIS, SEGMENT, Er, Error_dir, angle, b, data_path, Angle_suffix)
% Function computes a systematic error following magnitude and direction on
% the MARKERS and computes kinematics (PyCGM) for each error.

% Outputs:
% Angles: Structure containing kinematic data for each of the computations
% MM: Cell with names of virtual markers creates (for each error)

 %% 1. Run PyGCM2 on renamed files
 cd 'D:\Marker Misplacement Simulation\Test\Pelvic Misplacement\'
 counter= 0;
 dynamic_list = [];
 
for i = 1: length(C3D_filenames)
    patient_list = {}; %list of filenames relative to the static file
    if isempty(strfind(C3D_filenames{i},'SB'))==0
        %% 1.1 Find respective dynamic file
        static_file = C3D_filenames{i};
        patient_list = {static_file};
        c = 1;
        for j = 1:length(C3D_filenames)
            if isempty(strfind(C3D_filenames{i},C3D_filenames{j})) && isempty(strfind('G',C3D_filenames{j}(end-16)))==0
                if isempty(strfind(static_file(1,end-25:end-18),C3D_filenames{j}(1,end-25:end-18)))==0 && ~isempty(strfind(static_file(1,end-16:end-15),C3D_filenames{j}(1,end-16:end-15)))==0
                        patient_list{end+1} = C3D_filenames{j};
                        dynamic_list = [dynamic_list; string(C3D_filenames{j})];
                end
            end
        end   
        counter = counter +1;
        for s = 1:length(Er)
            %% 2. Define Error
            E = Er(s);
            if Error_dir == 'AP_ML'
                Error = [E, 0 , 0; 0, E, 0;  -E, 0, 0;  0, -E, 0];
            elseif Error_dir == 'AP_DP'
                Error = [E, 0 , 0; 0, 0, E; -E, 0, 0; 0, 0, -E];
            elseif Error_dir == 'ML_DP'
                Error = [0, E, 0;  0, 0, E;  0, -E, 0; 0, 0, -E];
            else
                disp('The error direction is not valid.')
            end
            
            % for the first iteration, calculate original data using PyCGM
            if s == 1
                %% 3 Run PyCGM2  % To add the joint centers and direction components for each segment (no need to be runned if already done)
                RunPyCGM2_ff(patient_list, data_path);
                %% 4.Save origin PyCGM angles (because translators are not working, save the original markers to restore after each calculation)
                % DYNAMIC
                dynamic_file = patient_list{2};
                acq = btkReadAcquisition(strcat(data_path, dynamic_file));
                Markers_original_d = btkGetMarkers(acq);
                btkClearAnalogs(acq);
                Angles(counter).original = btkGetAngles(acq);
%                 for mar = 1:length(MARK_to_MIS)
%                     btkAppendPoint(acq,'marker',strcat(MARK_to_MIS{mar},'_Original'),Markers_original_d.(MARK_to_MIS{mar})); % Optional (keep virtual misplaced markers)
%                 end
                btkWriteAcquisition(acq, dynamic_file);
                
                % STATIC
                acq = btkReadAcquisition(strcat(data_path, static_file));
                Markers_original_s = btkGetMarkers(acq);
                btkClearAnalogs(acq);
                Angles_static(counter).original = btkGetAngles(acq);
%                 for mar = 1:length(MARK_to_MIS)
%                     btkAppendPoint(acq,'marker',strcat(MARK_to_MIS{mar},'_Original'),Markers_original_s.(MARK_to_MIS{mar})); % Optional (keep virtual misplaced markers)
%                 end
                btkWriteAcquisition(acq, static_file);
            end
            
            %% Introduce virtual misplacement on the markers selected --> STATIC
            for j = 1:size(Error,1)
                tic
                % 5.1 Input misplacement on MARK_to_MIS
                acq=btkReadAcquisition(strcat(data_path, static_file));
                data_stat=btkGetMarkers(acq);
                btkWriteAcquisition(acq, static_file);
                
                acq=btkReadAcquisition(strcat(data_path, dynamic_file));
                data_dyn=btkGetMarkers(acq);
                btkWriteAcquisition(acq, dynamic_file);          
                
                [data_static,  MARKER_MIS, MM] = MISP_MARK (MARK_to_MIS, data_stat, SEGMENT, angle, j, E, Error_dir, s, Error);
                [data_dynamic] = MISP_MARK (MARK_to_MIS, data_dyn, SEGMENT, angle, j, E, Error_dir, s, Error);
             
                for mar = 1:length(MARK_to_MIS)                    
                    C3D_s.acq=btkReadAcquisition(strcat(data_path,static_file));
                    C3D_s.markers = btkGetMarkers(C3D_s.acq);
                    btkRemovePoint(C3D_s.acq, char(MARK_to_MIS{mar})); % Remove original point
                    btkAppendPoint(C3D_s.acq, 'marker', char(MARK_to_MIS{mar}),data_static.(MARKER_MIS{mar})); % Replace original point by the new misplaced point
                    btkAppendPoint(C3D_s.acq,'marker',char(MARKER_MIS{mar}),data_static.(MARKER_MIS{mar})); % Create a virtual marker with misplacement (optional)
                    btkWriteAcquisition(C3D_s.acq, static_file);
                    
                    C3D_d.acq=btkReadAcquisition(strcat(data_path,dynamic_file));
                    btkRemovePoint(C3D_d.acq, char(MARK_to_MIS{mar})); % Remove original point
                    btkAppendPoint(C3D_d.acq, 'marker', char(MARK_to_MIS{mar}),data_dynamic.(MARKER_MIS{mar})); % Replace original point by the new misplaced point
                    btkAppendPoint(C3D_d.acq,'marker',char(MARKER_MIS{mar}),data_dynamic.(MARKER_MIS{mar})); % Create a virtual marker with misplacement (optional)
                    btkWriteAcquisition(C3D_d.acq, dynamic_file);
                end
                
                to_delete = {'LHJC', 'RHJC', 'LKJC', 'RKJC', 'LAJC','RAJC', 'LFEMUR_X', 'LFEMUR_Y', 'LFEMUR_Z', 'PELVIS_X','PELVIS_Y','PELVIS_Z', 'LTIBIA_X','LTIBIA_Y','LTIBIA_Z', 'SACR', 'midASIS' };
                C3D_s.acq=btkReadAcquisition(strcat(data_path,static_file));
                for d = 1:length(to_delete)
                    btkRemovePoint(C3D_s.acq,to_delete{d}); %Remove calculated virtual points, if they already exist PyCGM won't recalculate them
                end
                btkWriteAcquisition(C3D_s.acq, static_file);
                
                C3D_d.acq=btkReadAcquisition(strcat(data_path,dynamic_file));
                for d = 1:length(to_delete)
                    btkRemovePoint(C3D_d.acq,to_delete{d});
                end
                btkWriteAcquisition(C3D_d.acq, dynamic_file);

                %% 7. Update suffix on CGM1_1.userSettings
                Update_UserSettings(MARKER_MIS)
                
                %% 8. Run PyGCM2
                commandStr1 = ['cd /d ' data_path];
                commandStr2 = ['python.exe ' 'pyCGM2_CGM11_modelling.py'];
                [status, commandOut] = system([commandStr1 ' & ' commandStr2],'-echo');
                disp(['----------------------------------------------']);
                disp(['End of computation for marker:', strcat(MARKER_MIS)]);
                disp(['----------------------------------------------']);
                
                delete('CGM1.1 [0].completeSettings')
                
                %% 9. Restore MARK1 data 
                acq = btkReadAcquisition(strcat(data_path,char(static_file)));
                for mar = 1:length(MARK_to_MIS)
                    btkRemovePoint(acq, MARK_to_MIS{mar});
                    btkAppendPoint(acq, 'marker',char(MARK_to_MIS{mar}),Markers_original_s.(MARK_to_MIS{mar}))
                end
                btkWriteAcquisition(acq, static_file);
                
                acq = btkReadAcquisition(strcat(data_path,char(dynamic_file)));
                for mar = 1:length(MARK_to_MIS)
                    btkRemovePoint(acq, MARK_to_MIS{mar});
                    btkAppendPoint(acq, 'marker',char(MARK_to_MIS{mar}),Markers_original_d.(MARK_to_MIS{mar}))
                end
                btkWriteAcquisition(acq, dynamic_file);
                toc
            end
            %% 10. Store kinematic data
            acq = btkReadAcquisition(strcat(data_path,char(dynamic_file)));
            Angles(counter).error = btkGetAngles(acq);
            btkWriteAcquisition(acq, dynamic_file);
            
            acq = btkReadAcquisition(strcat(data_path,char(static_file)));
            Angles_static(counter).error = btkGetAngles(acq);
            btkWriteAcquisition(acq, static_file);
            
        end
    end
end
                        