function [mean, SD] = Mean_Gait_Cycles2(C3D_filenames, C3D_path, static_file)

cd 'D:\Marker Misplacement Simulation\Test\Patient_1\'
%% 1. Original gait kinematics
%For each file, store kinematics of first cycle (left side)
figure(4)
for i = 1:length(C3D_filenames)
    acq = btkReadAcquisition(char(strcat(C3D_path, C3D_filenames{i})));
    Angles = btkGetAngles(acq);
    Events = btkGetEvents(acq);
    ff = btkGetFirstFrame(acq);
    HS_1 = (round(Events.Left_Foot_Strike(1)*100))-ff+1;
    HS_2 = (round(Events.Left_Foot_Strike(2)*100))-ff+1;
    HS_3 = (round(Events.Left_Foot_Strike(3)*100))-ff+1;
%     time = 0:100;
    x = 1:size(Angles.LHipAngles_PyCGM1(HS_1:HS_2,1),1);
    Hip_FE(:,i) = interp1(x',Angles.LHipAngles_PyCGM1(HS_1:HS_2,1), linspace(1,length(x),101));
    Hip_AA(:,i) = interp1(x',Angles.LHipAngles_PyCGM1(HS_1:HS_2,2), linspace(1,length(x),101));
    Hip_R(:,i) = interp1(x',Angles.LHipAngles_PyCGM1(HS_1:HS_2,3), linspace(1,length(x),101));
    
    Knee_FE(:,i) = interp1(x',Angles.LKneeAngles_PyCGM1(HS_1:HS_2,1), linspace(1,length(x),101));
    Knee_AA(:,i) = interp1(x',Angles.LKneeAngles_PyCGM1(HS_1:HS_2,2), linspace(1,length(x),101));
    Knee_R(:,i) = interp1(x',Angles.LKneeAngles_PyCGM1(HS_1:HS_2,3), linspace(1,length(x),101));
    
    Ankle_FE(:,i) = interp1(x',Angles.LAnkleAngles_PyCGM1(HS_1:HS_2,1), linspace(1,length(x),101));
    Ankle_AA(:,i) = interp1(x',Angles.LAnkleAngles_PyCGM1(HS_1:HS_2,2), linspace(1,length(x),101));
    Ankle_R(:,i) = interp1(x',Angles.LAnkleAngles_PyCGM1(HS_1:HS_2,3), linspace(1,length(x),101));
    
    xx = 1:size(Angles.LHipAngles_PyCGM1(HS_2:HS_3,1),1);
    Hip_FE(:,i+length(C3D_filenames)) = interp1(xx',Angles.LHipAngles_PyCGM1(HS_2:HS_3,1), linspace(1,length(xx),101));
    Hip_AA(:,i+length(C3D_filenames)) = interp1(xx',Angles.LHipAngles_PyCGM1(HS_2:HS_3,2), linspace(1,length(xx),101));
    Hip_R(:,i+length(C3D_filenames)) = interp1(xx',Angles.LHipAngles_PyCGM1(HS_2:HS_3,3), linspace(1,length(xx),101));
    
    Knee_FE(:,i+length(C3D_filenames)) = interp1(xx',Angles.LKneeAngles_PyCGM1(HS_2:HS_3,1), linspace(1,length(xx),101));
    Knee_AA(:,i+length(C3D_filenames)) = interp1(xx',Angles.LKneeAngles_PyCGM1(HS_2:HS_3,2), linspace(1,length(xx),101));
    Knee_R(:,i+length(C3D_filenames)) = interp1(xx',Angles.LKneeAngles_PyCGM1(HS_2:HS_3,3), linspace(1,length(xx),101));
    
    Ankle_FE(:,i+length(C3D_filenames)) = interp1(xx',Angles.LAnkleAngles_PyCGM1(HS_2:HS_3,1), linspace(1,length(xx),101));
    Ankle_AA(:,i+length(C3D_filenames)) = interp1(xx',Angles.LAnkleAngles_PyCGM1(HS_2:HS_3,2), linspace(1,length(xx),101));
    Ankle_R(:,i+length(C3D_filenames)) = interp1(xx',Angles.LAnkleAngles_PyCGM1(HS_2:HS_3,3), linspace(1,length(xx),101));  
end
    % calculate mean and std for each joint angles
    m_Hip_FE = mean(Hip_FE,2);
    sd_Hip_FE = std(Hip_FE,1,2);
    m_Hip_AA = mean(Hip_AA,2);
    sd_Hip_AA = std(Hip_AA,1,2);
    m_Hip_R = mean(Hip_R,2);
    sd_Hip_R = std(Hip_R,1,2);
    m_Knee_FE = mean(Knee_FE,2);
    sd_Knee_FE = std(Knee_FE,1,2);
    m_Knee_AA = mean(Knee_AA,2);
    sd_Knee_AA = std(Knee_AA,1,2);
    m_Knee_R = mean(Knee_R,2);
    sd_Knee_R = std(Knee_R,1,2);
    m_Ankle_FE = mean(Ankle_FE,2);
    sd_Ankle_FE = std(Ankle_FE,1,2);
    m_Ankle_AA = mean(Ankle_AA,2);
    sd_Ankle_AA = std(Ankle_AA,1,2);
    m_Ankle_R = mean(Ankle_R,2);
    sd_Ankle_R = std(Ankle_R,1,2);
    
%  %% 2. Simulated data
% 
% %% 2.1. Create virtual marker
% % rs with Defined error - Define before computation
% % MAR1: marker to misplace.
% % SEGMENT: segment where the marker is used to define the LCS.
% % Er: error magnitudes in mm.
% % Error_dir: 'AP_ML' antero-posteior + medial lateral (e.g. LKNE, LANK);'AP_DP' antero-posteior + proximal distal; 'ML_PD' medial lateral + proximal distal (e.g Lasi);
% % seg_origin: segment origin
% % 
% MARK1 = ('LKNE');
% Er = [5, 10, 15, 20, 30];
% Error_dir = 'AP';
% 
% SEGMENT.name = ('LFEMUR');
% SEGMENT.origin = ('LHJC');
% SEGMENT.proximal = [SEGMENT.name, '_Z'];
% SEGMENT.lateral  = [SEGMENT.name, '_Y'];
% SEGMENT.anterior = [SEGMENT.name, '_X'];
% b=1;
% angle = [0];
% 
% data_path = C3D_path;
% 
% %% 8. Run PyGCM2
% commandStr1 = ['cd /d ' data_path];
% commandStr2 = ['python.exe ' 'pyCGM2_CGM11_modelling.py'];
% [status, commandOut] = system([commandStr1 ' & ' commandStr2],'-echo');
% delete('CGM1.1 [0].completeSettings')
% % 2.1 Simulate marker misplacement
% 
% %% 2. DEFINE Error
% E = Er(5);
% % for mag = 1:length(Er)
%     Error = [E, 0, 0];
%     
%     %% 5. Create marker with error
%     
%     %% 5.1 Get MARK1 in STATIC file and store original MARK1 coordinates
%     acq=btkReadAcquisition(strcat(data_path, static_file));
%     data_static=btkGetMarkers(acq);
%     MARK1_original_static = data_static.(MARK1);
%     % btkWriteAcquisition(acq, static_file);
%     
%     %% 5.2 calculate LCS and add error to MARK1 in STATIC
%     MARK1_GCS     =  data_static.(MARK1);
%     MARK1_LCS     =  zeros(size(data_static.(SEGMENT.origin),1),4);
%     MARK1_Mis_LCS =  zeros(size(data_static.(SEGMENT.origin),1),3);
%     MARKER_MIS    =  [MARK1,'_', num2str(angle(1)),'_', num2str(E), '_', Error_dir];
%     data_static.(MARKER_MIS)  =  zeros(size(data_static.(SEGMENT.origin),1),4);
%     marker.(MARKER_MIS) = [];
%     
%     for m = 1:size(data_static.(SEGMENT.origin),1)
%         % Rotation matrix from GCS to LCS (3x3)
%         mat_left_fem(:,:,m) = Femur_Mat_Rot(data_static.(SEGMENT.origin)(m,:),data_static.(SEGMENT.proximal)(m,:),data_static.(SEGMENT.lateral)(m,:),data_static.(SEGMENT.anterior)(m,:) );
%         % Transformation matrix (4x4)
%         T = [mat_left_fem(:,1,m), mat_left_fem(:,2,m), mat_left_fem(:,3,m), (data_static.(SEGMENT.origin)(m,:))'; 0, 0, 0, 1];
%         Transf = Tinv_array3(T);
%         % Calculate marker coordinates in LCS
%         MARK1_LCS(m,:) = (Transf*[MARK1_GCS(m,:)'; 1])';
%         % Add an error on the marker in LCS
%         MARK1_Mis_LCS(m,:) = [MARK1_LCS(m,1)+Error(1,1), MARK1_LCS(m,2)+Error(1,2), MARK1_LCS(m,3)+Error(1,3)];
%         % Calculate marker coordinates in GCS
%         data_static.(MARKER_MIS)(m,:) = (inv(Transf)*[MARK1_Mis_LCS(m,:),1]')';
%     end
%     data_static.(MARKER_MIS)(:,4) = [];
%     to_delete = {'LKNE', 'LHJC', 'RHJC', 'LKJC', 'RKJC', 'LAJC','RAJC', 'LFEMUR_X', 'LFEMUR_Y', 'LFEMUR_Z', 'PELVIS_X','PELVIS_Y','PELVIS_Z', 'LTIBIA_X','LTIBIA_Y','LTIBIA_Z' };
%     acq=btkReadAcquisition(strcat(data_path, static_file));
%     
%     for d = 1:length(to_delete)
%         btkRemovePoint(acq,to_delete{d});
%     end
%     btkAppendPoint(acq,'marker','LKNE',data_static.(MARKER_MIS)); % replace the MARK1 by the modified one
%     
%     if isfield(data_static, 'original_LKNE')== 0
%         btkAppendPoint(acq,'marker','original_LKNE',data_static.(MARK1)); % Store on c3d the original MARK1
%     end
%     btkWriteAcquisition(acq, static_file);
%     
%     
%     % 5.3 read C3D in DYNAMIC
%     for i = 1:length(C3D_filenames)
%         dynamic_file = C3D_filenames{i};
%         acq  =  btkReadAcquisition(strcat(data_path, dynamic_file));
%         data_dynamic  =  btkGetMarkers(acq);
%         %     MARK1_original_dynamic(i).LKNE = data_dynamic.(MARK1);
%         btkWriteAcquisition(acq, dynamic_file);
%         
%         %% 5.4 calculate LCS and add error to MARK1 in Dynamic
%         MARK1_GCS     =  data_dynamic.(MARK1);
%         MARK1_LCS     =  zeros(size(data_dynamic.(SEGMENT.origin),1),4);
%         MARK1_Mis_LCS =  zeros(size(data_dynamic.(SEGMENT.origin),1),3);
%         data_dynamic.(MARKER_MIS)  =  zeros(size(data_dynamic.(SEGMENT.origin),1),4);
%         marker.(MARKER_MIS)  =  [];
%         
%         for m = 1:size(data_dynamic.(SEGMENT.origin),1)
%             mat_left_fem(:,:,m)  =  Femur_Mat_Rot(data_dynamic.(SEGMENT.origin)(m,:),data_dynamic.(SEGMENT.proximal)(m,:),data_dynamic.(SEGMENT.lateral)(m,:),data_dynamic.(SEGMENT.anterior)(m,:) );
%             T = [mat_left_fem(:,1,m), mat_left_fem(:,2,m), mat_left_fem(:,3,m), (data_dynamic.(SEGMENT.origin)(m,:))'; 0, 0, 0, 1];
%             Transf = Tinv_array3(T);
%             MARK1_LCS(m,:)  =  (Transf*[MARK1_GCS(m,:)'; 1])';
%             MARK1_Mis_LCS(m,:) =  [MARK1_LCS(m,1)+Error(1,1), MARK1_LCS(m,2)+Error(1,2), MARK1_LCS(m,3)+Error(1,3)];
%             data_dynamic.(MARKER_MIS)(m,:)  =  (inv(Transf)*[MARK1_Mis_LCS(m,:),1]')';
%         end
%         data_dynamic.(MARKER_MIS)(:,4)=[];
%         
%         %% 5.5 Delete joint centers and restore append virtual markers in the 2 files
%         
%         C3D_d.acq=btkReadAcquisition(strcat(data_path,dynamic_file));
%         for d = 1:length(to_delete)
%             btkRemovePoint(C3D_d.acq,to_delete{d});
%         end
%         
%         btkAppendPoint(C3D_d.acq,'marker','LKNE',data_dynamic.(MARKER_MIS)); % replace the MARK1 by the modified one
%         %     if isfield(data_dynamic, 'original_LKNE')== 0
%         %         btkAppendPoint(C3D_d.acq,'marker','original_LKNE',data_dynamic.(MARK1)); % Store on c3d the original MARK1
%         %     end
%         btkWriteAcquisition(C3D_d.acq, dynamic_file);
%         
%     end
%     %% 7. Update suffix on CGM1_1.userSettings
%     fid = fopen('CGM1_1.userSettings','r');
%     bb = 1;
%     tline = fgetl(fid);
%     AA{bb} = tline;
%     while ischar(tline)
%         bb = bb+1;
%         tline = fgetl(fid);
%         AA{bb} = tline;
%     end
%     fclose(fid);
%     change = (['    Point suffix: ',strcat(MARKER_MIS)]); % Change cell AA
%     AA{36} = strjoin(cellstr(change));
%     
%     % Write cell AA into txt
%     fid = fopen('CGM1_1.userSettings', 'w');
%     for t = 1:numel(AA)
%         if AA{t+1} == -1
%             fprintf(fid,'%s', AA{t});
%             break
%         else
%             fprintf(fid,'%s\n', AA{t});
%         end
%     end
%     fclose(fid);
%     %% 8. Run PyGCM2
%     commandStr1 = ['cd /d ' data_path];
%     commandStr2 = ['python.exe ' 'pyCGM2_CGM11_modelling.py'];
%     [status, commandOut] = system([commandStr1 ' & ' commandStr2],'-echo');
%     disp(['----------------------------------------------']);
%     disp(['End of computation for marker:', strcat(MARKER_MIS)]);
%     disp(['----------------------------------------------']);
%     
%     delete('CGM1.1 [0].completeSettings')
%     
%     % 9. Restore MARK1 data (to avoid error)
%     % STATIC FILE
%     acq = btkReadAcquisition(strcat(data_path,char(static_file)));
%     M = btkGetMarkers(acq);
%     btkRemovePoint(acq, 'LKNE');
%     btkAppendPoint(acq, 'marker','LKNE',MARK1_original_static)
%     btkWriteAcquisition(acq, static_file);
    %
% end
    % DYNAMIC FILES
%     dynamic_list = C3D_filenames;
%     magnitudes = {'5','10','15','20','30'}
%     for mag = 1:length(magnitudes)
%         for i = 1: length(C3D_filenames)
%             acq = btkReadAcquisition(strcat(C3D_path,char(C3D_filenames{i})));
%             %         M = btkGetMarkers(acq);
%             %         btkRemovePoint(acq, 'LKNE');
%             %         btkAppendPoint(acq, 'marker','LKNE',MARK1_original_dynamic(i).LKNE);
%             Angles = btkGetAngles(acq);
%             
%             Events = btkGetEvents(acq);
%             ff = btkGetFirstFrame(acq);
%             HS_1 = (round(Events.Left_Foot_Strike(1)*100))-ff+1;
%             HS_2 = (round(Events.Left_Foot_Strike(2)*100))-ff+1;
%             
%             x = 1:size(Angles.LHipAngles_PyCGM1(HS_1:HS_2,1),1);
%             Hip_FE_e(:,i) = interp1(x',Angles.(strcat('LHipAngles_LKNE_0_',magnitudes{mag},'_AP'))(HS_1:HS_2,1), linspace(1,length(x),101));
%             Hip_AA_e(:,i) = interp1(x',Angles.LHipAngles_LKNE_0_10_AP(HS_1:HS_2,2), linspace(1,length(x),101));
%             Hip_R_e(:,i) = interp1(x',Angles.LHipAngles_LKNE_0_10_AP(HS_1:HS_2,3), linspace(1,length(x),101));
%             
%             Knee_FE_e(:,i) = interp1(x',Angles.LKneeAngles_LKNE_0_10_AP(HS_1:HS_2,1), linspace(1,length(x),101));
%             Knee_AA_e(:,i) = interp1(x',Angles.LKneeAngles_LKNE_0_10_AP(HS_1:HS_2,2), linspace(1,length(x),101));
%             Knee_R_e(:,i) = interp1(x',Angles.LKneeAngles_LKNE_0_10_AP(HS_1:HS_2,3), linspace(1,length(x),101));
%             
%             Ankle_FE_e(:,i) = interp1(x',Angles.LAnkleAngles_LKNE_0_10_AP(HS_1:HS_2,1), linspace(1,length(x),101));
%             Ankle_AA_e(:,i) = interp1(x',Angles.LAnkleAngles_LKNE_0_10_AP(HS_1:HS_2,2), linspace(1,length(x),101));
%             Ankle_R_e(:,i) = interp1(x',Angles.LAnkleAngles_LKNE_0_10_AP(HS_1:HS_2,3), linspace(1,length(x),101));
%             
%             %         btkWriteAcquisition(acq, dynamic_file);
%         end
%     end
%     m_Hip_FE_e = mean(Hip_FE_e,2);
%     m_Hip_AA_e = mean(Hip_AA_e,2);
%     m_Hip_R_e = mean(Hip_R_e,2);
%     
%     m_Knee_FE_e = mean(Knee_FE_e,2);
%     m_Knee_AA_e = mean(Knee_AA_e,2);
%     m_Knee_R_e = mean(Knee_R_e,2);
%     
%     m_Ankle_FE_e = mean(Ankle_FE_e,2);
%     m_Ankle_AA_e = mean(Ankle_AA_e,2);
%     m_Ankle_R_e = mean(Ankle_R_e,2);
    LineColor = ['b', 'c', 'g', 'm', 'r'];
    Mag = [5,10,15,20,30];
    for i = 1:length(Mag)
        subplot(3,3,1)
        if i == 1
            corridor(m_Hip_FE,sd_Hip_FE,'k',[0:100])
            hold on
            plot(m_Hip_FE, 'k')
        end
        plot( m_Hip_FE + 3.472* (Mag(i)/10), 'Color', LineColor(i))
        plot(m_Hip_FE - 3.64 * (Mag(i)/10) , 'LineStyle', '--', 'Color', LineColor(i))
        xlim([0 100])
        ylim([-35 85])
        ylabel('Angle (�)')
        title('Hip Flexion-Extension')
        
        subplot(3,3,2)
        if i == 1
            corridor(m_Hip_AA,sd_Hip_AA,'k',[0:100])
            hold on
            plot(m_Hip_AA, 'k')
        end
        plot(m_Hip_AA + 1.54* (Mag(i)/10), 'Color', LineColor(i))
        plot(m_Hip_AA - 0.84* (Mag(i)/10) , 'LineStyle', '--', 'Color', LineColor(i))
        xlim([0 100])
        ylim([-20 20])
        title('Hip Adduction-Abduction')
        
        subplot(3,3,3)
        if i == 1
            corridor(m_Hip_R,sd_Hip_R,'k',[0:100])
            hold on
            plot(m_Hip_R, 'k')
        end
        plot(m_Hip_R - 7.523* (Mag(i)/10), 'Color', LineColor(i))
        plot(m_Hip_R + 7.566* (Mag(i)/10) , 'LineStyle', '--', 'Color', LineColor(i))
        xlim([0 100])
        ylim([-45 25])
        title('Hip Internal-External Rotation')
        
        subplot(3,3,4)
        if i == 1
            corridor(m_Knee_FE,sd_Knee_FE,'k',[0:100])
            hold on
            plot(m_Knee_FE, 'k')
        end
        plot(m_Knee_FE + 5.436* (Mag(i)/10), 'Color', LineColor(i))
        plot(m_Knee_FE - 6.27* (Mag(i)/10) , 'LineStyle', '--', 'Color', LineColor(i))
        xlim([0 100])
        ylim([-35 85])
        ylabel('Angle (�)')
        title('Knee Flexion-Extension')
        
        subplot(3,3,5)
        if i ==1
            corridor(m_Knee_AA,sd_Knee_AA,'k',[0:100])
            hold on
            plot(m_Knee_AA, 'k')
        end
        plot(m_Knee_AA + 3.4342* (Mag(i)/10), 'Color', LineColor(i))
        plot(m_Knee_AA - 3.01* (Mag(i)/10) , 'LineStyle', '--', 'Color', LineColor(i))
        xlim([0 100])
        ylim([-20 20])
        title('Knee Adduction-Abduction')
        
        subplot(3,3,6)
        if i == 1
            corridor(m_Knee_R,sd_Knee_R,'k',[0:100])
            hold on
            plot(m_Knee_R, 'k')
        end     
        plot(m_Knee_R - 1.267* (Mag(i)/10),'Color', LineColor(i))
        plot(m_Knee_R + 1.53* (Mag(i)/10) , 'LineStyle', '--', 'Color', LineColor(i))
        xlim([0 100])
        ylim([-30 30])
        title('Knee Internal-External Rotation')
        
        subplot(3,3,7)
        if i == 1
            corridor(m_Ankle_FE,sd_Ankle_FE,'k',[0:100])
            hold on
            plot(m_Ankle_FE, 'k')
        end
        plot(m_Ankle_FE + 1.872* (Mag(i)/10),'Color', LineColor(i))
        plot(m_Ankle_FE - 2.416* (Mag(i)/10) , 'LineStyle', '--', 'Color', LineColor(i))
        xlim([0 100])
        ylim([-35 85])
        xlabel('Gait cycle (%)')
        ylabel('Angle (�)')
        title('Ankle Flexion-Extension')
        
        subplot(3,3,8)
        if i == 1
            corridor(m_Ankle_AA,sd_Ankle_AA,'k',[0:100])
            hold on
            plot(m_Ankle_AA, 'k')
        end
        plot(m_Ankle_AA - 0.991* (Mag(i)/10), 'Color', LineColor(i))
        plot(m_Ankle_AA + 1.06*(Mag(i)/10) , 'LineStyle', '--', 'Color', LineColor(i))
        xlim([0 100])
        ylim([-20 20])
        xlabel('Gait cycle (%)')
        title('Ankle Adduction-Abduction')
        
        subplot(3,3,9)
        if i == 1
            corridor(m_Ankle_R,sd_Ankle_R,'k',[0:100])
            hold on
            plot(m_Ankle_R, 'k')
        end
        plot(m_Ankle_R + 5.758* (Mag(i)/10), 'Color', LineColor(i))
        plot(m_Ankle_R - 6.455* (Mag(i)/10) , 'LineStyle', '--', 'Color', LineColor(i))
        xlim([0 100])
        ylim([-40 25])
        xlabel('Gait cycle (%)')
        title('Ankle Internal-External Rotation')
    
    end


end