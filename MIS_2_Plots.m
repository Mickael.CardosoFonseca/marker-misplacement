cd 'D:\Marker Misplacement Simulation\Test\Test_2\'

load Angles
%% 1. Calculate RMSD tables

% compute the results and export RMSD, std and max and mean(over patients)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_RMSD(Angles, Error, Er, MM, counter); 

% save Angles
% writetable(T_RMSD, 'RMSD_LKNE.xls')
% writetable(T_m_RMSD, 'mean_RMSD_LKNE.xls')
% writetable(T_m_max, 'mean_maxRMSD_LKNE.xls')
% writetable(T_m_RMSD_pMagn, 'm_RMSD_pMagn_LKNE.xls')
HipFlex_1 = vec2mat(m_RMSD_pMagn(:,1),length(Error));
HipFlex_1 = [HipFlex_1(:,1) fliplr(HipFlex_1(:,2:8))];
HipAdd_1  = vec2mat(m_RMSD_pMagn(:,2),length(Error));
HipAdd_1 = [HipAdd_1(:,1) fliplr(HipAdd_1(:,2:8))];
HipRot_1  = vec2mat(m_RMSD_pMagn(:,3),length(Error));
HipRot_1 = [HipRot_1(:,1) fliplr(HipRot_1(:,2:8))];

KneeFlex_1 = vec2mat(m_RMSD_pMagn(:,4),length(Error));
KneeFlex_1 = [KneeFlex_1(:,1) fliplr(KneeFlex_1(:,2:8))];
KneeAdd_1  = vec2mat(m_RMSD_pMagn(:,5),length(Error));
KneeAdd_1 = [KneeAdd_1(:,1) fliplr(KneeAdd_1(:,2:8))];
KneeRot_1  = vec2mat(m_RMSD_pMagn(:,6),length(Error));
KneeRot_1 = [KneeRot_1(:,1) fliplr(KneeRot_1(:,2:8))];

AnkleFlex_1 = vec2mat(m_RMSD_pMagn(:,7),length(Error));
AnkleFlex_1 = [AnkleFlex_1(:,1) fliplr(AnkleFlex_1(:,2:8))];
AnkleAdd_1  = vec2mat(m_RMSD_pMagn(:,8),length(Error));
AnkleAdd_1 = [AnkleAdd_1(:,1) fliplr(AnkleAdd_1(:,2:8))];
AnkleRot_1  = vec2mat(m_RMSD_pMagn(:,9),length(Error));
AnkleRot_1 = [AnkleRot_1(:,1) fliplr(AnkleRot_1(:,2:8))];
%% 2. Polar plots
Lable = {'     Ant', 'Ant + Dist', 'Dist', 'Post + Dist', 'Post', 'Post + Prox', 'Prox', 'Ant + Prox'};
LineColor = {'b', 'c', 'g', 'm', 'r'};
LineStyle = {'no', ':'};
LevelNum = 5;
maximo = 17;
figure(1)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_norm(Angles, Error, Er, MM, dynamic_file, data_path, 'cycle')
MIS_PlotPolar(m_RMSD_pMagn, Error)

figure(20)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_norm(Angles, Error, Er, MM, dynamic_file, data_path, 'stance')
MIS_PlotPolar(m_RMSD_pMagn, Error)

figure(21)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_norm(Angles, Error, Er, MM, dynamic_file, data_path, 'swing')
MIS_PlotPolar(m_RMSD_pMagn, Error)


%% Table1.
[RR, Table1] = MIS_table_RMSD(Angles, Error, Er, MM, counter, data_path, C3D_filenames); 
writetable(Table1, 'Table Results RMSD.xls');
%% 15. Boxplot
% mis_bplot(Angles, Error, Er, MM, counter, neg_dir, pos_dir)

%% 16. Polar plot
figure(1)
MIS_PlotPolar(m_RMSD_pMagn, Error)
Lable = {'     Ant', 'Ant + Dist', 'Dist', 'Post + Dist', 'Post', 'Post + Prox', 'Prox', 'Ant + Prox'};
LineColor = {'b', 'c', 'g', 'm', 'r'};
LineStyle = {'no', ':'};
LevelNum = 5;
maximo = 17;

% Hip Flexion
figure (1)
MIS_Radar_Plot(HipFlex_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Hip Flexion / Extension')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(1), 'Hip Flexion.pdf')
% Hip Add/Abd
figure (2)
MIS_Radar_Plot(HipAdd_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Hip Ab/Adduction')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(2), 'Hip Add.pdf')
% Hip Rot
figure (3)
MIS_Radar_Plot(HipRot_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Hip Int/External Rotation')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
legend('5 mm', '10 mm', '15 mm', '20 mm', '30 mm')
saveas (figure(3), 'Hip Rot.pdf')
% knee Flexion
figure (4)
MIS_Radar_Plot(KneeFlex_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Knee Flex/Extension')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(4), 'Knee Flex.pdf')
% Knee Add/Abd
figure (5)
MIS_Radar_Plot(KneeAdd_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Knee Ab/Adduction')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(5), 'Knee Add.pdf')
% Knee Rot
figure (6)
MIS_Radar_Plot(KneeRot_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Knee Int/External Rotation')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(6), 'Knee Rot.pdf')
% Ankle Flexion
figure (7)
MIS_Radar_Plot(AnkleFlex_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Ankle Flex/Extension')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(7), 'Ankle Flex.pdf')
% Ankle Add
figure (8)
MIS_Radar_Plot(AnkleAdd_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Ankle Ab/Adduction')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(8), 'Ankle Add.pdf')
% Ankle Flexion
figure (9)
MIS_Radar_Plot(AnkleRot_1,Lable,LineColor,LineStyle,LevelNum, maximo)
t=title('Ankle Int/External Rotation')
t.FontSize = 16
set(t,'position',get(t,'position')-[0 -0.5 0])
saveas (figure(9), 'Ankle Rot.pdf')


%% 17. Scatter plot (RMSD�/Error normalized)
 anthro = MIS_anthropometric_data(C3D_filenames, data_path);                 % get anthropometric data
 Leg_length = zeros(1, length(anthro.subject));
 Knee_Width = zeros(1, length(anthro.subject));
 for i = 1:length(anthro.subject)
     Leg_length(i) = str2double(anthro.subject(i).Left_LegLength_mm);
     Knee_Width(i) = str2double(anthro.subject(i).Left_KneeWidth_mm);
 end
 figure(2)
 [correl_LL, C, Table2] = MIS_Correlation(Leg_length, R, Er, 6, 22, 'Leg length', 10);   % correl = correlation between  error and leg_length normalized; norm_errors:
 
 figure(3)
 [correl_KW, C, Table2] = MIS_Correlation(Knee_Width, R, Er, 40, 25, 'Knee Width');
 writetable(Table2, 'Table Correlation RMSD Leg Length.xls');

%% 18. Remove mean
figure (4)
C3D_filename = '10_00543_04508_20171107-GBNNN-VDEF-14.C3D';
R_Offset = MIS_RemoveOffset(Angles, Error, Er, MM, 10, data_path, C3D_filename)

figure (5)
C3D_filename7 = '07_02452_04602_20180403-GBNNN-VDEF-11.C3D';
R_Offset = MIS_RemoveOffset(Angles, Error, Er, MM, 7, data_path, C3D_filename7)

figure (6)
C3D_filename3 = '03_02595_04561_20180202-GBNNN-VDEF-09.C3D';
R_Offset = MIS_RemoveOffset(Angles, Error, Er, MM, 3, data_path, C3D_filename3)


%% 19. Plot Curves
subject = 9;
LineColor = {'b', 'c', 'g', 'm', 'r'};

% Plot Curves containing all misplacements * magnitudes
% Hip
figure(7)
MIS_PlotCurves(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, 'Hip')

% Knee
figure(8)
MIS_PlotCurves(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, 'Knee')

% Ankle
figure(9)
MIS_PlotCurves(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, 'Ankle')

% Plot Curves containing only one direction (e.g. ant + post) * magnitudes
Angle1 = 0;
Angle2 = 180;

figure(10)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Hip', 'Ant Post')

figure(11)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Knee', 'Ant Post')

figure(12)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Ankle', 'Ant Post')
%%
subject = 10;
Angle1 = 0;
Angle2 = 180;
figure (4)
MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Hip', 'Ant Post', 1)
MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Knee', 'Ant Post', 2)
MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Ankle', 'Ant Post', 3)
% MIS_PlotCurves3(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'FootProgress', 'Ant Post', 3)

%%
Angle1 = 90;
Angle2 = 270;
figure(13)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Hip', 'Prox Dist')

figure(14)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Knee', 'Prox Dist')

figure(15)
MIS_PlotCurves2(Angles, Er, MM, subject, data_path, C3D_filename, LineColor, Angle1, Angle2, MARK1, 'Ankle', 'Prox Dist')

%% Save figures
saveas (figure(1), 'Polar Plot.pdf')
saveas (figure(2), 'Scatter RMSD/LegLength.pdf')
saveas (figure(3), 'Scatter RMSD/KneeWidth.pdf')
saveas (figure(4), 'Angle_Res_Pat10_AntPost.pdf')

%% Plot RMSD/Knee Flexion
% Not important !
figure(11)      
hold on
for j=1:10
    KFlex_orig    = Angles(j).original.LKneeAngles_PyCGM1(:,1);
    KFlex_err_10  = Angles(j).error.LKneeAngles_LKNE_0_10_AP_ML(:,1);
    KFlex_RMSD_10 = sqrt((KFlex_err_10(:,1) - KFlex_orig(:,1)).^2);
    
    % Table normalized error magnitude by leg length
    %KFlex_err_10_n = (KFlex_err_10/max(KFlex_err_10))*100;
%     for i = 1:length(KFlex_err_10_n)
        plot(KFlex_err_10,KFlex_RMSD_10,'.');
%     end
    clear KFlex_orig KFlex_err_10 KFlex_RMSD_10
end
%% Plot Scatter RMSD/%Leg length  all patients (anterior direction)
 anthro = MIS_anthropometric_data(C3D_filenames, data_path);                 % get anthropometric data
 Leg_length = zeros(1, length(anthro.subject));
 Knee_Width = zeros(1, length(anthro.subject));
 for i = 1:length(anthro.subject)
     Leg_length(i) = str2double(anthro.subject(i).Left_LegLength_mm);
     Knee_Width(i) = str2double(anthro.subject(i).Left_KneeWidth_mm);
 end
MIS_Scatter_RMSD_LL(Angles, Er, Leg_Length, )