function [DATA,  MARKER_MIS, MM] = MISP_MARK (M_to_MIS, DATA, SEGMENT, angle, j, E, Error_dir, s, Error)
for mar = 1:length(M_to_MIS)
    MARK_GCS = DATA.(M_to_MIS{mar});
    MARK_LCS = zeros(size(DATA.(SEGMENT.origin),1),4);
    MARK_MIS_LCS = zeros(size(DATA.(SEGMENT.origin),1),3);
    MARKER_MIS{mar}   =  [M_to_MIS{mar},'_', num2str(angle(j)),'_', num2str(E), '_', Error_dir];
    MM{s,j}      =  MARKER_MIS{mar};
    DATA.(MARKER_MIS{mar})  =  zeros(size(DATA.(SEGMENT.origin),1),4);
    marker.(MARKER_MIS{mar}) = [];
    
    for m = 1:size(DATA.(SEGMENT.origin),1)
        % Rotation matrix from GCS to LCS (3x3)
        mat_left_fem(:,:,m) = Femur_Mat_Rot(DATA.(SEGMENT.origin)(m,:),DATA.(SEGMENT.proximal)(m,:),DATA.(SEGMENT.lateral)(m,:),DATA.(SEGMENT.anterior)(m,:) );
        % Transformation matrix (4x4)
        T = [mat_left_fem(:,1,m), mat_left_fem(:,2,m), mat_left_fem(:,3,m), (DATA.(SEGMENT.origin)(m,:))'; 0, 0, 0, 1];
        Transf = Tinv_array3(T);
        % Calculate marker coordinates in LCS
        MARK_LCS(m,:) = (Transf*[MARK_GCS(m,:)'; 1])';
        % Add an error on the marker in LCS
        MARK_MIS_LCS(m,:) = [MARK_LCS(m,1)+Error(j,1), MARK_LCS(m,2)+Error(j,2), MARK_LCS(m,3)+Error(j,3)];
        % Calculate marker coordinates in GCS
        DATA.(MARKER_MIS{mar})(m,:) = (inv(Transf)*[MARK_MIS_LCS(m,:),1]')';
    end
    DATA.(MARKER_MIS{mar})(:,4)= [];
end
end