clear all
close all
clc

%% 1. Create virtual marke
% rs with Defined error - Define before computation
% MAR1: marker to misplace.
% SEGMENT: segment where the marker is used to define the LCS.
% Er: error magnitudes in mm.
% Error_dir: 'AP_ML' antero-posteior + medial lateral;'AP_DP' antero-posteior + proximal distal; 'ML_PD' medial lateral + proximal distal ;
% seg_origin: segment origin
tic
MARK1 = ('LKNE');
SEGMENT = ('LFEMUR');
Er = [5, 10, 15, 20, 30];
Error_dir = 'AP_ML';
seg_origin=['LHJC'];
seg_prox=[SEGMENT, '_Z'];
seg_lat=[SEGMENT, '_Y'];
seg_ant=[SEGMENT, '_X'];
b=1;
angle = [0; 45; 90; 135; 180; 225; 270; 315];

%% 2. Select files
original_datapath = 'D:\Marker Misplacement Simulation\Test\Original_Data\';
data_path  = 'D:\Marker Misplacement Simulation\Test\Test_2\';
patients   = dir([data_path,'*.c3d']);
[C3D_filenames,C3D_path, FilterIndex]=uigetfile({'*.C3D'},'Sélectionner les ficihers C3D ou GCD',['D:\Marker Misplacement Simulation\Test\Original_Data\' '/'],'MultiSelect','on');
cd 'D:\Marker Misplacement Simulation\Test\Original_Data\'
C3D_path = data_path;

% %% 3. Move and rename files + Crop static files 
% for i=1:length(C3D_filenames)
%     C3D_file = char(C3D_filenames{i});
%     copyfile(C3D_file,C3D_path);
% %     if isempty(strfind(C3D_file,'SB'))==0
% %         acq=btkReadAcquisition(strcat(C3D_path,C3D_file));
% %         first_frame = btkGetFirstFrame(acq);
% %         btkCropAcquisition(acq, first_frame, first_frame+50);
% %         btkWriteAcquisition(acq, C3D_file);
% %     end
%     C3D_filenames{i} = C3D_file;
% end

for s = 1:length(Er)
    cd 'D:\Marker Misplacement Simulation\Test\Original_Data\'
    for i=1:length(C3D_filenames)
        C3D_file = char(C3D_filenames{i});
        copyfile(C3D_file,C3D_path);
        %     if isempty(strfind(C3D_file,'SB'))==0
        %         acq=btkReadAcquisition(strcat(C3D_path,C3D_file));
        %         first_frame = btkGetFirstFrame(acq);
        %         btkCropAcquisition(acq, first_frame, first_frame+50);
        %         btkWriteAcquisition(acq, C3D_file);
        %     end
        C3D_filenames{i} = C3D_file;
    end
    counter= 0;
%% 4. DEFINE Error
    E = Er(s);
    if Error_dir == 'AP_DP'
        Error = [E*cos(0), E*sin(0) , 0; E*cos(45), E*sin(45), 0; 0, E, 0; -E*cos(45), E*sin(45), 0; -E, 0, 0; ...
            -E*cos(45), -E*sin(45), 0; 0, -E, 0; E*cos(45), -E*cos(45), 0];
    elseif Error_dir == 'AP_ML'
        Error = [E, 0 , 0; E*sin(45), 0, E*cos(45); 0, 0, E; -E*sin(45),0, E*cos(45); -E, 0, 0; ...
            -E*sin(45), 0, -E*cos(45); 0, 0, -E; E*sin(45), 0, -E*sin(45)];
    elseif Error_dir == 'ML_DP'
        Error = [0, E, 0; 0, E*cos(45), E*sin(45); 0, 0, E; 0, -E*cos(45), E*sin(45); 0, -E, 0; ...
            0, -E*cos(45), -E*sin(45); 0, 0, -E; 0, E*cos(45), -E*sin(45)];
    else
        disp('The error direction is not valid.')
    end
    
    %% 5. Run PyGCM2 on renamed files
    cd 'D:\Marker Misplacement Simulation\Test\Test_2\'
    for i = 1: length(C3D_filenames)
        patient_list = {}; %list of filenames relative to the static file
        if isempty(strfind(C3D_filenames{i},'SB'))==0
            %% Find respective dynamic file
            static_file = C3D_filenames{i};
            patient_list = {static_file};
            c = 1;
            for j = 1:length(C3D_filenames)
                if isempty(strfind(C3D_filenames{i},C3D_filenames{j}))
                    if isempty(strfind(static_file(1,end-25:end-18),C3D_filenames{j}(1,end-25:end-18)))==0 && ~isempty(strfind(static_file(1,end-16:end-15),C3D_filenames{j}(1,end-16:end-15)))==0
                        if isempty(strfind('G',C3D_filenames{j}(end-16)))==0
                            patient_list{end+1} = C3D_filenames{j};
                        end
                    end
                end
            end
            %% 6. Store MARK1 original data as 'MARK1_Original' - optional
            for p= 1:length(patient_list)
                acq=btkReadAcquisition(strcat(C3D_path, char(patient_list(p))));
                markers = btkGetMarkers(acq);
                if ~isfield(markers, ([MARK1, '_ORIGINAL']))
                    btkAppendPoint(acq,'marker',char([MARK1,'_ORIGINAL']),markers.(MARK1));
                end
                btkWriteAcquisition(acq, char(patient_list(p)));
            end
            %% 5.1 Run PyCGM2
            % To add the joint centers and direction components for each segment (no need to be runned if already done)
            RunPyCGM2_ff(patient_list, C3D_path);
            counter = counter +1;
            %% 6. Get origin angles
            if s == 1
                dynamic_file = patient_list{2};
                acq=btkReadAcquisition(strcat(C3D_path, dynamic_file));
                Angles(counter).original = btkGetAngles(acq);
            end

            %% 7. Create marker with error
            for j=1:size(Error,1)                
                %% 7.1 read C3D in STATIC
                acq=btkReadAcquisition(strcat(C3D_path, static_file));
                data_static=btkGetMarkers(acq);

                %% 7.2 calculate LCS and add error to MARK1 in STATIC
                MARK1_GCS     =  data_static.([MARK1, '_ORIGINAL']);
                MARK1_LCS     =  zeros(size(data_static.(seg_origin),1),4);
                MARK1_Mis_LCS =  zeros(size(data_static.(seg_origin),1),3);
                MARKER_MIS    =  [MARK1,'_', num2str(angle(j)),'_', num2str(E), '_', Error_dir];
                MM{s,j}         =  MARKER_MIS;
                data_static.(MARKER_MIS)  =  zeros(size(data_static.(seg_origin),1),4);
                marker.(MARKER_MIS) = [];

                for m = 1:size(data_static.(seg_origin),1)
                    % Rotation matrix from GCS to LCS (3x3)
                    mat_left_fem(:,:,m) = Femur_Mat_Rot(data_static.(seg_origin)(m,:),data_static.(seg_prox)(m,:),data_static.(seg_lat)(m,:),data_static.(seg_ant)(m,:) );
                    % Transformation matrix (4x4)
                    Transf = [mat_left_fem(:,1,m), mat_left_fem(:,2,m), mat_left_fem(:,3,m), (data_static.(seg_origin)(m,:))'; 0, 0, 0, 1];
                    % Calculate marker coordinates in LCS
                    MARK1_LCS(m,:) = (Transf*[MARK1_GCS(m,:)'; 1])';
                    % Add an error on the marker in LCS
                    MARK1_Mis_LCS(m,:) = [MARK1_LCS(m,1)+Error(j,1), MARK1_LCS(m,2)+Error(j,2), MARK1_LCS(m,3)+Error(j,3)];
                    % Calculate marker coordinates in GCS
                    data_static.(MARKER_MIS)(m,:) = (inv(Transf)*[MARK1_Mis_LCS(m,:),1]')';
                end
                data_static.(MARKER_MIS)(:,4) = [];

                %% 7.3 read C3D in DYNAMIC
                dynamic_file = patient_list{2};
                acq  =  btkReadAcquisition(strcat(C3D_path, dynamic_file));
                data_dynamic  =  btkGetMarkers(acq);

                %% 7.4 calculate LCS and add error to MARK1 in Dynamic
                MARK1_GCS     =  data_dynamic.([MARK1, '_ORIGINAL']);
                MARK1_LCS     =  zeros(size(data_dynamic.(seg_origin),1),4);
                MARK1_Mis_LCS =  zeros(size(data_dynamic.(seg_origin),1),3);
                data_dynamic.(MARKER_MIS)  =  zeros(size(data_dynamic.(seg_origin),1),4);
                marker.(MARKER_MIS)  =  [];

                for m = 1:size(data_dynamic.(seg_origin),1)
                    mat_left_fem(:,:,m)  =  Femur_Mat_Rot(data_dynamic.(seg_origin)(m,:),data_dynamic.(seg_prox)(m,:),data_dynamic.(seg_lat)(m,:),data_dynamic.(seg_ant)(m,:) );
                    Transf  =  [mat_left_fem(:,1,m), mat_left_fem(:,2,m), mat_left_fem(:,3,m), (data_dynamic.(seg_origin)(m,:))'; 0, 0, 0, 1];
                    MARK1_LCS(m,:)  =  (Transf*[MARK1_GCS(m,:)'; 1])';
                    MARK1_Mis_LCS(m,:) =  [MARK1_LCS(m,1)+Error(j,1), MARK1_LCS(m,2)+Error(j,2), MARK1_LCS(m,3)+Error(j,3)];
                    data_dynamic.(MARKER_MIS)(m,:)  =  (inv(Transf)*[MARK1_Mis_LCS(m,:),1]')';
                end
                data_dynamic.(MARKER_MIS)(:,4)=[];


                %% 7.5 Replace the marker MARK1 by the virtual marker (with error)
                to_delete = {'LHJC', 'RHJC', 'LKJC', 'RKJC', 'LAJC','RAJC'};

                C3D_s.acq=btkReadAcquisition(strcat(C3D_path,static_file));
                btkRemovePoint(C3D_s.acq,char(MARK1));
                btkAppendPoint(C3D_s.acq,'marker',char(MARK1),data_static.(MARKER_MIS));
                for d = 1:length(to_delete)
                    btkRemovePoint(C3D_s.acq,to_delete{d});
                end
                btkWriteAcquisition(C3D_s.acq, static_file);

                C3D_d.acq=btkReadAcquisition(strcat(C3D_path,dynamic_file));
                btkRemovePoint(C3D_d.acq,char(MARK1));
                btkAppendPoint(C3D_d.acq,'marker',char(MARK1),data_dynamic.(MARKER_MIS));
                for d = 1:length(to_delete)
                    btkRemovePoint(C3D_d.acq,to_delete{d});
                end
                btkWriteAcquisition(C3D_d.acq, dynamic_file);


                %% 8. Update suffix on CGM1_1.userSettings
                fid = fopen('CGM1_1.userSettings','r');
                bb = 1;
                tline = fgetl(fid);
                AA{bb} = tline;
                while ischar(tline)
                    bb = bb+1;
                    tline = fgetl(fid);
                    AA{bb} = tline;
                end
                fclose(fid);
                change = (['    Point suffix: ',strcat(MARKER_MIS)]); % Change cell AA
                AA{36} = strjoin(cellstr(change));

                % Write cell AA into txt
                fid = fopen('CGM1_1.userSettings', 'w');
                for t = 1:numel(AA)
                    if AA{t+1} == -1
                        fprintf(fid,'%s', AA{t});
                        break
                    else
                        fprintf(fid,'%s\n', AA{t});
                    end
                end
                fclose(fid);

                %% 9. Run PyGCM2
                commandStr1 = ['cd /d ' C3D_path];
                commandStr2 = ['python.exe ' 'pyCGM2_CGM11_modelling.py'];
                [status, commandOut] = system([commandStr1 ' & ' commandStr2],'-echo');
                disp(['----------------------------------------------']);
                disp(['End of computation for marker:', strcat(MARKER_MIS)]);
                disp(['----------------------------------------------']);
                
                %% 10. Restore the value of LKNE by its original
                for p= 1:length(patient_list)
                    acq=btkReadAcquisition(strcat(C3D_path, char(patient_list(p))));
                    markers = btkGetMarkers(acq);
                    btkRemovePoint(acq, MARK1);
                    m_or = [MARK1, '_ORIGINAL'];
                    btkAppendPoint(acq,'marker', MARK1, markers.(m_or));
                    btkWriteAcquisition(acq, char(patient_list(p)));
                end
            end
            %% 10. Store kinematic data
            acq = btkReadAcquisition(strcat(C3D_path,char(dynamic_file)));
            Angles(counter).error = btkGetAngles(acq);
        end    
    end
end


%% 11. Calculate RMSD
% compute the results and export RMSD, std and max and mean(over patients)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max]  = res_RMSD(Angles, Error, Er, MM, counter); 

writetable(T_RMSD, 'RMSD.xls')
% writetable(T_std, 'RMSD.xls')
writetable(T_max, 'RMSD.xls')
writetable(T_m_RMSD, 'mean_RMSD.xls')


