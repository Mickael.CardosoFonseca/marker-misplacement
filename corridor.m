function []=corridor(mean,std,color,x_in)
% Trace le corridor moyenne +/- ecart type
% mean, std = column vector
% x column vector[0:end,end:-1:0]
% Pour le trace des corridors, permet de faire un aller retour sur le
% graphe
n=size(mean,1);
if nargin == 3
    x = [0:1:n-1 n-1:-1:0]';
elseif nargin ==4
    x = [x_in x_in(end:-1:1)]';
end
y=[mean+std;mean(end:-1:1)-std(end:-1:1)]; % [Aller;Retour]
A=fill(x,y,color,'LineStyle','none','FaceAlpha',0.3); % Trace le corridor +/- 1 SD
set(get(get(A,'Annotation'),'LegendInformation'),'IconDisplayStyle','off'); % Retire surface de la l�gende